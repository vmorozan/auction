import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TestFiltersComponent } from './test-filters/test-filters.component';

const routes: Routes = [
  {
    path: 'account', loadChildren:() => import('./core/components/account/account.module')
      .then(mod => mod.AccountModule), data: {breadcrumb: {skip: true}}
  },
  {
    path: 'test', component: TestFiltersComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
