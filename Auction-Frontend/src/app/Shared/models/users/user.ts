export class User {
    userName: string;
    email: string;
    token: string;

    /**
     *
     */
    constructor() {
        this.userName = '';
        this.email = '';
        this.token = '';
    }
}
