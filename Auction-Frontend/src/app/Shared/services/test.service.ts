import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TestService {

  baseURL = environment.urlApi;

  constructor(private http: HttpClient) { }

  testFilters(){
    return this.http.get<any>(this.baseURL + 'filter/categories');
    }
}
