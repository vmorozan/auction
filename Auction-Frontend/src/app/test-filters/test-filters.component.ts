import { Component, OnInit } from '@angular/core';
import { TestService } from '../shared/services/test.service';

@Component({
  selector: 'test-filters',
  templateUrl: './test-filters.component.html',
  styleUrls: ['./test-filters.component.scss']
})
export class TestFiltersComponent implements OnInit {

  token = localStorage.getItem('token') ?? null;
  categories: any[] = [];
  constructor(private testService: TestService) { }

  ngOnInit(): void {
  }

  submit(){
    this.testService.testFilters()
      .subscribe(res => {
        this.categories = res;
        console.log(res);
      }, error => {
        console.log('Something wrong with token!!')
      })
  }

}
