import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AccountService } from 'src/app/shared/services/account.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;

  constructor(private accountService: AccountService) { 
    this.form = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required, Validators.minLength(6)]),
    })
  }

  ngOnInit() {
    
  }

  submit(){
    this.accountService.login(this.form.value)
      .subscribe(() => {
        console.log('token from server: ', localStorage.getItem('token'))
      }, error => {
        console.log('Something wrong!!!')
      })
  }

}
