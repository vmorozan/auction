import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { EnsureModuleLoadedOnceGuard } from './ensure-module-loaded-once-guard';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from './interceptors/jwt.interceptor';
import { AccountComponent } from './components/account/account.component';
import { AccountModule } from './components/account/account.module';
import { AccountRoutingModule } from './components/account/account-routing.module';



@NgModule({
  declarations: [
    NavBarComponent,
    AccountComponent
  ],
  imports: [
    AccountModule,
    AccountRoutingModule,
    CommonModule
  ],
  exports:[
    NavBarComponent,
    AccountModule,
    AccountRoutingModule
  ],
  providers:[
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}
  ]
})
export class CoreModule extends EnsureModuleLoadedOnceGuard{ 
    /**
     *
     */
    constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
      super(parentModule);
      
    }

}
