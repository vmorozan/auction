import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-sidebar-list',
  templateUrl: './sidebar-list.component.html',
  styleUrls: ['./sidebar-list.component.scss']
})
export class SidebarListComponent implements OnInit {

  @Output() public sidenavClose = new EventEmitter();
  isExpanded: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }


  onSidenavClose = () => {
    this.sidenavClose.emit(this.isExpanded);
  }

}
