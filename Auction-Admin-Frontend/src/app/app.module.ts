import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserModule } from './users/users.module';
import { HomeComponent } from './home/home.component';
import { MaterialModule } from './material/material.module';
import { HeaderComponent } from './navigation/header/header.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SidebarListComponent } from './navigation/sidebar-list/sidebar-list.component';
import { AccountModule } from './account/account.module';
import { ExecutorComponent } from './executor/executor.component';
import { FilterComponent } from './filters/filter.component';
import { FilterModule } from './filters/filter.module';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    SidebarListComponent,
    ExecutorComponent,
    FilterComponent
  ],
  imports: [
    CoreModule,
    BrowserModule,
    HttpClientModule,
    MaterialModule,
    UserModule,
    FilterModule,
    AccountModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
