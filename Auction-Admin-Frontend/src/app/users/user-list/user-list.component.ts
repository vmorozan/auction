import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UserList } from 'src/app/shared/models/users/userList';
import { UserService } from 'src/app/shared/services/user.service';
import { UserFilter } from 'src/app/shared/models/filters/users/user.filter';
import { Guid } from 'guid-typescript';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  displayedColumns: string[] = ['username', 'email', 'firstname', 'lastname', 'street', 'city', 'action'];
  userList: UserList[] = [];
  dataSource = new MatTableDataSource(this.userList);
  searchData: string = '';
  pageEvent!: PageEvent;
  filter = new UserFilter();
  searchValue = '';
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private userService: UserService
  ) { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.getsInit();
  }

  applyFilter(event: Event) {
    this.filter.searchParam = (event.target as HTMLInputElement).value;
    this.gets();
  }

  getServerData(event:PageEvent){
    this.filter.currentPage = event?.pageIndex;
    this.filter.itemsPerPage = event?.pageSize;
    this.gets();
  }

  gets(){
    let filterJson = JSON.stringify(this.filter);
    this.userService.gets(filterJson).subscribe(users => {
      this.dataSource.data = users.objects;
      setTimeout(() => {
        this.dataSource.paginator!.length = users.totalItems;
        this.dataSource.paginator!.pageIndex = users.currentPage;
        this.dataSource.paginator!.pageSize = users.itemsPerPage;
      }, 10);
    });
  }

  getsInit(){
    let filterJson = JSON.stringify(this.filter);
    this.userService.gets(filterJson).subscribe(users => {
      this.dataSource = new MatTableDataSource(users.objects);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  sortData(sort: Sort) {
    this.filter.sortingDirection = sort.direction;
    this.filter.sortingParam = sort.active;
    this.gets();
  }

  editUserDialog(user: UserList) {
    console.log(user);
  }

  deleteUserDialog(user: UserList) {
    this.userService.delete(Guid.parse(user.key)).subscribe(res => console.log(res));
  }

}
