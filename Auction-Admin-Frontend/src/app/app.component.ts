import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  sidebarWidth: string = '50px';
  contentSidenavMarginLeft: string = '50px';
  constructor() {
  }

  ngOnInit(){
  }

  expendSidebar($event: any) {
    this.sidebarWidth = $event == false ? '50px' : '250px';
    this.contentSidenavMarginLeft = $event == false ? '50px' : '250px';
  }
}
