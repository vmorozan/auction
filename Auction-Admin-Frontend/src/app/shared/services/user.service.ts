import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { UserList } from '../models/users/userList';
import { ResultDTO } from '../models/DTOs/ResultDTO';
import { Guid } from 'guid-typescript';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private baseURL = environment.urlApi;

  constructor(private http: HttpClient, private router: Router) { }

  gets(filter: string){
    return this.http.get<ResultDTO<UserList>>(`${this.baseURL}user/gets?filter=${filter}`);
  }

  delete(key: Guid){
    return this.http.delete<ResultDTO<UserList>>(`${this.baseURL}user/delete?key=${key}`);
  }
}
