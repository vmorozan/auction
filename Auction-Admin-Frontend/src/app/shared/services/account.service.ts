import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/users/user';
import { map } from 'rxjs/operators'
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  baseURL = environment.urlApi;
  user = new User();
  private currentUserSource = new BehaviorSubject<User>(this.user);
  currentUser$ = this.currentUserSource.asObservable();

  constructor(private http: HttpClient, private router: Router) { }

  login(values: any){
    return this.http.post<User>(this.baseURL + 'account/login', values)
        .pipe(map(
          (user: User) => {
            if (user) {
              localStorage.setItem('token', user.token);
              this.currentUserSource.next(user);
            }
          }
        ));
    }

  register(values: any){
    return this.http.post<User>(this.baseURL + 'account/register', values)
        .pipe(map(
          (user: User) => {
            if (user) {
              localStorage.setItem('token', user.token);
            }
          }
        ));
  }

  logout(){
    localStorage.removeItem('token');
    this.currentUserSource.next(this.user);
    this.router.navigateByUrl('/');
  }
}
