import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { City } from '../models/filters/city';
import { CommonFilterListDTO } from '../models/common-filters/common-filters/common-filter-list-dto';
import { ResultDTO } from '../models/DTOs/ResultDTO';
import { Result } from '../models/result';
import { CommonFilterDTO } from '../models/common-filters/common-filters/common-filter-dto';
import { CategoryDTO } from '../models/common-filters/categories/category-dto';
import { SubCategoryDTO } from '../models/common-filters/sub-categories/sub-category-dto';
import { CategoryListDTO } from '../models/common-filters/categories/category-list-dto';
import { Guid } from 'guid-typescript';

@Injectable({
  providedIn: 'root'
})
export class FilterService {
  private baseURL = environment.urlApi;

  constructor(private http: HttpClient, private router: Router) { }

  getCities(){
    return this.http.get<City[]>(this.baseURL + 'filter/cities');
  }

  getCategories() {
    return this.http.get<Result<CategoryListDTO[]>>(this.baseURL + 'filter/categories/gets');
  }

  getListCategories(filter: string) {
    return this.http.get<Result<ResultDTO<CategoryDTO>>>(`${this.baseURL}filter/list-categories/gets?filter=${filter}`);
  }

  addCategory(categoryDTO: CategoryDTO) {
    return this.http.post<Result<boolean>>(`${this.baseURL}filter/category/add`, categoryDTO);
  }

  addSubCategory(categoryDTO: SubCategoryDTO) {
    return this.http.post<Result<boolean>>(`${this.baseURL}filter/sub-category/add`, categoryDTO);
  }

  getSubCategories(categoryKey: Guid) {
    return this.http.get<Result<SubCategoryDTO[]>>(this.baseURL + `filter/sub-categories/gets?categoryKey=${categoryKey}`);
  }

  getSubListCategories(filter: string) {
    return this.http.get<Result<ResultDTO<SubCategoryDTO>>>(`${this.baseURL}filter/list-sub-categories/gets?filter=${filter}`);
  }

  getCommonFilters(filter: string){
    return this.http.get<Result<ResultDTO<CommonFilterListDTO>>>(`${this.baseURL}filter/common-filter/gets?commonFilter=${filter}`);
  }

  addCommonFilters(filterDTO: CommonFilterDTO){
    return this.http.post<Result<boolean>>(`${this.baseURL}filter/common-filter/add`, filterDTO);
  }

}
