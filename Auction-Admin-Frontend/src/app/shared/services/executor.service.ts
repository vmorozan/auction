import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { ResultDTO } from '../models/DTOs/ResultDTO';
import { Guid } from 'guid-typescript';
import { ExecutorList } from '../models/executors/executorList';
import { Executor } from '../models/executors/executor';
import { Result } from '../models/result';

@Injectable({
  providedIn: 'root'
})
export class ExecutorService {
  private baseURL = environment.urlApi;

  constructor(private http: HttpClient, private router: Router) { }

  gets(filter: string){
    return this.http.get<Result<ResultDTO<ExecutorList>>>(`${this.baseURL}executor/gets?filter=${filter}`);
  }

  delete(key: Guid){
    return this.http.delete<Result<ResultDTO<boolean>>>(`${this.baseURL}executor/delete?key=${key}`);
  }

  add(model: Executor){
    return this.http.post<Result<ResultDTO<boolean>>>(`${this.baseURL}executor/add`, model);
  }
}
