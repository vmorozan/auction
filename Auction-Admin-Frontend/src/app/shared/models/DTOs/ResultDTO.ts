export class ResultDTO<T> {
    objects: T[];
    itemsPerPage: number;
    totalItems: number;
    currentPage: number;

    constructor() {
        this.objects = [];
        this.itemsPerPage = 5;
        this.totalItems = 1;
        this.currentPage = 0;
    }

}