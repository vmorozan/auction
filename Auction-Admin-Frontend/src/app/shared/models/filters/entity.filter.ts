export class EntityFilter {
    sortingParam: string;
    searchParam: string;
    sortingDirection: string;
    currentPage: number;
    itemsPerPage: number;

    constructor(){
        this.currentPage = 0;
        this.searchParam = '';
        this.itemsPerPage = 5;
        this.sortingParam = '';
        this.sortingDirection = ''
    }
}