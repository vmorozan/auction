import { CategoryDTO } from "../categories/category-dto";

export class SubCategoryDTO {
    key!: string;
    categoryName!: string;
    categoryKey!: string;
    name!: string;
}