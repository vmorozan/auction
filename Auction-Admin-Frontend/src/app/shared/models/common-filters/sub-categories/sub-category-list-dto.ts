export class SubCategoryListDTO {
    key!: string;
    categoryName!: string;
    name!: string;
}