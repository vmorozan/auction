export class CategoryDTO {
    key!: string;
    id!: string;
    name!: string;
}