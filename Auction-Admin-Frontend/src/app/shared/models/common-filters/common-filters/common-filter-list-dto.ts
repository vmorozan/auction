import { Base } from "../../base";

export class CommonFilterListDTO extends Base{
    categoryName!: string;
    subCategoryName!: string;
    name!: string;
    value!: string;
}