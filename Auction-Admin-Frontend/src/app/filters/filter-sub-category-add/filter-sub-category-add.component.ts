import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CategoryListDTO } from 'src/app/shared/models/common-filters/categories/category-list-dto';
import { FilterService } from 'src/app/shared/services/filter.service';

@Component({
  selector: 'app-filter-sub-category-add',
  templateUrl: './filter-sub-category-add.component.html',
  styleUrls: ['./filter-sub-category-add.component.scss']
})
export class FilterSubCategoryAddComponent implements OnInit {

  subCategoryForm: FormGroup;
  categories!: CategoryListDTO[];


  constructor(
    private filterService: FilterService,
    private router:Router) { 
    this.subCategoryForm = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      categoryKey: new FormControl(null, [Validators.required])
    })
  }

  ngOnInit(): void {
    this.filterService.getCategories().subscribe(list => this.categories = list.resultObject);

  }


  submit(){
    this.filterService.addSubCategory(this.subCategoryForm.value)
      .subscribe(() => {
        this.router.navigate(['/filters/sub-categories/list']);
      }, error => {
        console.log('Something wrong!!!')
      })
  }

}
