import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterSubCategoryAddComponent } from './filter-sub-category-add.component';

describe('FilterSubCategoryAddComponent', () => {
  let component: FilterSubCategoryAddComponent;
  let fixture: ComponentFixture<FilterSubCategoryAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilterSubCategoryAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterSubCategoryAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
