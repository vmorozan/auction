import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { CategoryDTO } from 'src/app/shared/models/common-filters/categories/category-dto';
import { CategoryFilter } from 'src/app/shared/models/filters/categories/category.filter';
import { FilterService } from 'src/app/shared/services/filter.service';

@Component({
  selector: 'app-filter-category-list',
  templateUrl: './filter-category-list.component.html',
  styleUrls: ['./filter-category-list.component.scss']
})
export class FilterCategoryListComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'action'];
  categoryList: CategoryDTO[] = [];
  dataSource = new MatTableDataSource(this.categoryList);
  searchData: string = '';
  pageEvent!: PageEvent;
  filter = new CategoryFilter();
  searchValue = '';
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;


  constructor(
    private filterService: FilterService
  ) { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.getsInit();
  }

  applyFilter(event: Event) {
    this.filter.searchParam = (event.target as HTMLInputElement).value;
    this.gets();
  }

  getServerData(event:PageEvent){
    this.filter.currentPage = event?.pageIndex;
    this.filter.itemsPerPage = event?.pageSize;
    this.gets();
  }

  gets(){
    let filterJson = JSON.stringify(this.filter);
    this.filterService.getListCategories(filterJson).subscribe(filters => {
      if (filters.isOK){
        this.dataSource.data = filters.resultObject.objects;
        setTimeout(() => {
          this.dataSource.paginator!.length = filters.resultObject.totalItems;
          this.dataSource.paginator!.pageIndex = filters.resultObject.currentPage;
          this.dataSource.paginator!.pageSize = filters.resultObject.itemsPerPage;
        }, 10);
      }
      
    });
  }

  getsInit(){
    let filterJson = JSON.stringify(this.filter);
    this.filterService.getListCategories(filterJson).subscribe(filters => {
      if (filters.isOK){
        this.dataSource = new MatTableDataSource(filters.resultObject.objects);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        setTimeout(() => {
          this.dataSource.paginator!.length = filters.resultObject.totalItems;
          this.dataSource.paginator!.pageIndex = filters.resultObject.currentPage;
          this.dataSource.paginator!.pageSize = filters.resultObject.itemsPerPage;
        }, 10);
      }
      
    });
  }

  sortData(sort: Sort) {
    this.filter.sortingDirection = sort.direction;
    this.filter.sortingParam = sort.active;
    this.gets();
  }

  // editUserDialog(user: CommonFilterListDTO) {
  //   console.log(user);
  // }

}
