import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterCategoryListComponent } from './filter-category-list.component';

describe('FilterCategoryListComponent', () => {
  let component: FilterCategoryListComponent;
  let fixture: ComponentFixture<FilterCategoryListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilterCategoryListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterCategoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
