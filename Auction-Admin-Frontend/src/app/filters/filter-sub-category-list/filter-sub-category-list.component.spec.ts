import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterSubCategoryListComponent } from './filter-sub-category-list.component';

describe('FilterSubCategoryListComponent', () => {
  let component: FilterSubCategoryListComponent;
  let fixture: ComponentFixture<FilterSubCategoryListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilterSubCategoryListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterSubCategoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
