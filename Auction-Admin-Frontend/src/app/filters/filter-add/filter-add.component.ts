import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Guid } from 'guid-typescript';
import { CategoryListDTO } from 'src/app/shared/models/common-filters/categories/category-list-dto';
import { SubCategoryDTO } from 'src/app/shared/models/common-filters/sub-categories/sub-category-dto';
import { FilterService } from 'src/app/shared/services/filter.service';

@Component({
  selector: 'app-filter-add',
  templateUrl: './filter-add.component.html',
  styleUrls: ['./filter-add.component.scss']
})
export class FilterAddComponent implements OnInit {

  filterForm: FormGroup;
  categories!: CategoryListDTO[];
  subCategories!: SubCategoryDTO[];


  constructor(
    private filterService: FilterService,
    private router:Router) { 
    this.filterForm = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      value: new FormControl(null, [Validators.required]),
      categoryKey: new FormControl(null),
      subCategoryKey: new FormControl(null)
    })
  }

  ngOnInit(): void {
    this.filterService.getCategories().subscribe(list => this.categories = list.resultObject);

  }


  submit(){
    this.filterService.addCommonFilters(this.filterForm.value)
      .subscribe(() => {
        this.router.navigate(['/filters/list']);
      }, error => {
        console.log('Something wrong!!!')
      })
  }

  callSubCategory(){
    this.filterService.getSubCategories(Guid.parse(this.filterForm.get('categoryKey')?.value)).subscribe(list => this.subCategories = list.resultObject);
  }

}
