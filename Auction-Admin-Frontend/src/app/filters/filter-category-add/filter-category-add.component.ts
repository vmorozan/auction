import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FilterService } from 'src/app/shared/services/filter.service';

@Component({
  selector: 'app-filter-category-add',
  templateUrl: './filter-category-add.component.html',
  styleUrls: ['./filter-category-add.component.scss']
})
export class FilterCategoryAddComponent implements OnInit {

  categoryForm: FormGroup;


  constructor(
    private filterService: FilterService,
    private router:Router) { 
    this.categoryForm = new FormGroup({
      name: new FormControl(null, [Validators.required])
    })
  }

  ngOnInit(): void {

  }


  submit(){
    this.filterService.addCategory(this.categoryForm.value)
      .subscribe(() => {
        this.router.navigate(['/filters/categories/list']);
      }, error => {
        console.log('Something wrong!!!')
      })
  }

}
