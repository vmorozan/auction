import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterCategoryAddComponent } from './filter-category-add.component';

describe('FilterCategoryAddComponent', () => {
  let component: FilterCategoryAddComponent;
  let fixture: ComponentFixture<FilterCategoryAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilterCategoryAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterCategoryAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
