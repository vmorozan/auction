import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { CommonFilterListDTO } from 'src/app/shared/models/common-filters/common-filters/common-filter-list-dto';
import { EntityFilter } from 'src/app/shared/models/filters/entity.filter';
import { FilterService } from 'src/app/shared/services/filter.service';

@Component({
  selector: 'app-filter-list',
  templateUrl: './filter-list.component.html',
  styleUrls: ['./filter-list.component.scss']
})
export class FilterListComponent implements OnInit {

  displayedColumns: string[] = ['name', 'value', 'categoryName', 'subCategoryName', 'action'];
  commonFilterList: CommonFilterListDTO[] = [];
  dataSource = new MatTableDataSource(this.commonFilterList);
  searchData: string = '';
  pageEvent!: PageEvent;
  filter = new EntityFilter();
  searchValue = '';
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;


  constructor(
    private filterService: FilterService
  ) { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.getsInit();
  }

  applyFilter(event: Event) {
    this.filter.searchParam = (event.target as HTMLInputElement).value;
    this.gets();
  }

  getServerData(event:PageEvent){
    this.filter.currentPage = event?.pageIndex;
    this.filter.itemsPerPage = event?.pageSize;
    this.gets();
  }

  gets(){
    let filterJson = JSON.stringify(this.filter);
    this.filterService.getCommonFilters(filterJson).subscribe(filters => {
      if (filters.isOK){
        this.dataSource.data = filters.resultObject.objects;
        setTimeout(() => {
          this.dataSource.paginator!.length = filters.resultObject.totalItems;
          this.dataSource.paginator!.pageIndex = filters.resultObject.currentPage;
          this.dataSource.paginator!.pageSize = filters.resultObject.itemsPerPage;
        }, 10);
      }
      
    });
  }

  getsInit(){
    let filterJson = JSON.stringify(this.filter);
    this.filterService.getCommonFilters(filterJson).subscribe(filters => {
      if (filters.isOK){
        this.dataSource = new MatTableDataSource(filters.resultObject.objects);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        setTimeout(() => {
          this.dataSource.paginator!.length = filters.resultObject.totalItems;
          this.dataSource.paginator!.pageIndex = filters.resultObject.currentPage;
          this.dataSource.paginator!.pageSize = filters.resultObject.itemsPerPage;
        }, 10);
      }
      
    });
  }

  sortData(sort: Sort) {
    this.filter.sortingDirection = sort.direction;
    this.filter.sortingParam = sort.active;
    this.gets();
  }

  editUserDialog(user: CommonFilterListDTO) {
    console.log(user);
  }

  // deleteUserDialog(user: CommonFilterListDTO) {
  //   this.filterService.delete(Guid.parse(user.key)).subscribe(res => console.log(res));
  // }

}
