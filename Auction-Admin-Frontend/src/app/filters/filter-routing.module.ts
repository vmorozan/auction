import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FilterListComponent } from './filter-list/filter-list.component';
import { FilterAddComponent } from './filter-add/filter-add.component';
import { FilterCategoryListComponent } from './filter-category-list/filter-category-list.component';
import { FilterCategoryAddComponent } from './filter-category-add/filter-category-add.component';
import { FilterSubCategoryListComponent } from './filter-sub-category-list/filter-sub-category-list.component';
import { FilterSubCategoryAddComponent } from './filter-sub-category-add/filter-sub-category-add.component';


const routes: Routes = [
  {
      path: 'list', component: FilterListComponent
  },
  {
      path: 'add', component: FilterAddComponent
  },
  {
      path: 'categories/list', component: FilterCategoryListComponent
  },
  {
      path: 'categories/add', component: FilterCategoryAddComponent
  },
  {
      path: 'sub-categories/list', component: FilterSubCategoryListComponent
  },
  {
      path: 'sub-categories/add', component: FilterSubCategoryAddComponent
  }
];

@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule]
})
export class FilterRoutingModule { }
