import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterListComponent } from './filter-list/filter-list.component';
import { FilterAddComponent } from './filter-add/filter-add.component';
import { FilterRoutingModule } from './filter-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FilterSubCategoryListComponent } from './filter-sub-category-list/filter-sub-category-list.component';
import { FilterSubCategoryAddComponent } from './filter-sub-category-add/filter-sub-category-add.component';
import { FilterCategoryAddComponent } from './filter-category-add/filter-category-add.component';
import { FilterCategoryListComponent } from './filter-category-list/filter-category-list.component';



@NgModule({
  declarations: [
    FilterListComponent,
    FilterAddComponent,
    FilterSubCategoryListComponent,
    FilterSubCategoryAddComponent,
    FilterCategoryAddComponent,
    FilterCategoryListComponent,
  ],
  imports: [
    CommonModule,
    FilterRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule
  ]
})
export class FilterModule { }
