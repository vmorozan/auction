import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: '', component: HomeComponent
  },
  {
    path: 'account', loadChildren:() => import('./account/account.module')
      .then(mod => mod.AccountModule), data: {breadcrumb: {skip: true}}
  },
  {
    path: 'users', loadChildren:() => import('./users/users.module')
      .then(mod => mod.UserModule)
  },
  {
    path: 'executors', loadChildren:() => import('./executor/executor.module')
      .then(mod => mod.ExecutorModule)
  },
  {
    path: 'filters', loadChildren:() => import('./filters/filter.module')
      .then(mod => mod.FilterModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
