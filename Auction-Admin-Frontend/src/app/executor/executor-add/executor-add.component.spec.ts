import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExecutorAddComponent } from './executor-add.component';

describe('ExecutorAddComponent', () => {
  let component: ExecutorAddComponent;
  let fixture: ComponentFixture<ExecutorAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExecutorAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutorAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
