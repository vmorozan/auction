import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Executor } from 'src/app/shared/models/executors/executor';
import { ExecutorService } from 'src/app/shared/services/executor.service';

@Component({
  selector: 'app-executor-add',
  templateUrl: './executor-add.component.html',
  styleUrls: ['./executor-add.component.scss']
})
export class ExecutorAddComponent implements OnInit {

  executorForm: FormGroup;

  constructor(
    private executorService: ExecutorService,
    private router:Router) { 
    this.executorForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      firstName: new FormControl(null, [Validators.required]),
      lastName: new FormControl(null, [Validators.required]),
      city: new FormControl(null, [Validators.required]),
      zipCode: new FormControl(null, [Validators.required]),
      street: new FormControl(null, [Validators.required]),
      executorNumber: new FormControl(null, [Validators.required]),
    })
  }

  ngOnInit(): void {
  }


  submit(){
    this.executorService.add(this.executorForm.value)
      .subscribe(() => {
        this.router.navigate(['/executors/list']);
      }, error => {
        console.log('Something wrong!!!')
      })
  }
}
