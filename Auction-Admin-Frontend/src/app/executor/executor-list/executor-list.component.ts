import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Guid } from 'guid-typescript';
import { ExecutorList } from 'src/app/shared/models/executors/executorList';
import { ExecutorFilter } from 'src/app/shared/models/filters/executors/executor.filter';
import { ExecutorService } from 'src/app/shared/services/executor.service';

@Component({
  selector: 'app-executor-list',
  templateUrl: './executor-list.component.html',
  styleUrls: ['./executor-list.component.scss']
})
export class ExecutorListComponent implements OnInit {

  displayedColumns: string[] = ['firstname', 'lastname', 'street', 'city', 'executorNumber', 'action'];
  userList: ExecutorList[] = [];
  dataSource = new MatTableDataSource(this.userList);
  searchData: string = '';
  pageEvent!: PageEvent;
  filter = new ExecutorFilter();
  searchValue = '';
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;


  constructor(
    private executorService: ExecutorService
  ) { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.getsInit();
  }

  applyFilter(event: Event) {
    this.filter.searchParam = (event.target as HTMLInputElement).value;
    this.gets();
  }

  getServerData(event:PageEvent){
    this.filter.currentPage = event?.pageIndex;
    this.filter.itemsPerPage = event?.pageSize;
    this.gets();
  }

  gets(){
    let filterJson = JSON.stringify(this.filter);
    this.executorService.gets(filterJson).subscribe(executors => {
      if (executors.isOK){
        this.dataSource.data = executors.resultObject.objects;
        setTimeout(() => {
          this.dataSource.paginator!.length = executors.resultObject.totalItems;
          this.dataSource.paginator!.pageIndex = executors.resultObject.currentPage;
          this.dataSource.paginator!.pageSize = executors.resultObject.itemsPerPage;
        }, 10);
      }
      
    });
  }

  getsInit(){
    let filterJson = JSON.stringify(this.filter);
    this.executorService.gets(filterJson).subscribe(executors => {
      if (executors.isOK){
        this.dataSource = new MatTableDataSource(executors.resultObject.objects);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
      
    });
  }

  sortData(sort: Sort) {
    this.filter.sortingDirection = sort.direction;
    this.filter.sortingParam = sort.active;
    this.gets();
  }

  editUserDialog(user: ExecutorList) {
    console.log(user);
  }

  deleteUserDialog(user: ExecutorList) {
    this.executorService.delete(Guid.parse(user.key)).subscribe(res => console.log(res));
  }

}
