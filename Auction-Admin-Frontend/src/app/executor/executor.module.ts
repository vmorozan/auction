import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExecutorListComponent } from './executor-list/executor-list.component';
import { ExecutorAddComponent } from './executor-add/executor-add.component';
import { ExecutorsRoutingModule } from './executor-routing.module';
import { MaterialModule } from '../material/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ExecutorListComponent,
    ExecutorAddComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ExecutorsRoutingModule,
    FlexLayoutModule
  ]
})
export class ExecutorModule { }
