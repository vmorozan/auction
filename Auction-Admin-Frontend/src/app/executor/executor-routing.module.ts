import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExecutorAddComponent } from './executor-add/executor-add.component';
import { ExecutorListComponent } from './executor-list/executor-list.component';

const routes: Routes = [
    {
        path: 'list', component: ExecutorListComponent
    },
    {
        path: 'add', component: ExecutorAddComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExecutorsRoutingModule { }
