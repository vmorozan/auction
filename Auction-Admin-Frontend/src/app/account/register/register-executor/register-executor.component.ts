import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { City } from 'src/app/shared/models/filters/city';
import { AccountService } from 'src/app/shared/services/account.service';
import { FilterService } from 'src/app/shared/services/filter.service';

@Component({
  selector: 'app-register-executor',
  templateUrl: './register-executor.component.html',
  styleUrls: ['./register-executor.component.scss']
})
export class RegisterExecutorComponent implements OnInit {

  cities: City[] = [];

  registerForm: FormGroup;
  constructor(
    private filterService: FilterService,
    private accountService: AccountService) {
    this.registerForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      firstName: new FormControl(null, [Validators.required]),
      lastName: new FormControl(null, [Validators.required]),
      street: new FormControl(null, [Validators.required]),
      city: new FormControl(null, [Validators.required]),
      license: new FormControl(null, [Validators.required]),
    });
   }

  ngOnInit(): void {
    this.filterService.getCities().subscribe(list => this.cities = list)
  }

  submit() {
    this.accountService.register(this.registerForm.value)
    .subscribe(() => {
      console.log('token from server: ', localStorage.getItem('token'))
    }, error => {
      console.log('Something wrong!!!')
    })
  }

}
