import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'account', loadChildren:() => import('./account/account.module')
      .then(mod => mod.AccountModule), data: {breadcrumb: {skip: true}}
  },
  {
    path: 'sequester', loadChildren:() => import('./sequesters/sequester.module')
      .then(mod => mod.SequesterModule), data: {breadcrumb: {skip: true}}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
