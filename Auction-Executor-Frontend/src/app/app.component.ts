import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
sidebarWidth: string = '50px';
  contentSidenavMarginLeft: string = '50px';
  constructor() {
  }

  ngOnInit(){
  }

  expendSidebar($event: any) {
    this.sidebarWidth = $event == false ? '50px' : '250px';
    this.contentSidenavMarginLeft = $event == false ? '50px' : '250px';
  }

}
