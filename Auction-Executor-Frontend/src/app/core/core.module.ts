import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EnsureModuleLoadedOnceGuard } from './ensure-module-loaded-once-guard';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from './interceptors/jwt.interceptor';



@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule
  ],
  providers:[
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}
  ]
})
export class CoreModule extends EnsureModuleLoadedOnceGuard{ 
    /**
     *
     */
    constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
      super(parentModule);
      
    }

}
