import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AccountService } from 'src/app/shared/services/account.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output() public sidenavToggle = new EventEmitter();
  token = localStorage.getItem('token') ?? null;

  constructor(
    private accountService: AccountService
  ) { }

  ngOnInit(): void {
  }


  onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }

  logout(){
    this.accountService.logout();
    this.token = null;
  }
}
