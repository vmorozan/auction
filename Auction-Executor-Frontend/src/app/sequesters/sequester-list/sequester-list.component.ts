import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AuctionType } from 'src/app/shared/models/enums/auction-type.enum';
import { SequesterFilter } from 'src/app/shared/models/filters/sequesters/sequester.filter';
import { SequesterDTO } from 'src/app/shared/models/sequesters/sequester.model';
import { SequesterService } from 'src/app/shared/services/sequester.service';

@Component({
  selector: 'app-sequester-list',
  templateUrl: './sequester-list.component.html',
  styleUrls: ['./sequester-list.component.scss'],
})
export class SequesterListComponent implements OnInit {

  displayedColumns: string[] = ['name', 'description', 'status', 'type', 'markedPrice', 'buyedPrice', 'action'];
  sequesterCars!: SequesterDTO[];
  dataSource = new MatTableDataSource(this.sequesterCars);
  searchData: string = '';
  pageEvent!: PageEvent;
  filter = new SequesterFilter();
  searchValue = '';
  AuctionType = AuctionType;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;


  constructor(
    private sequesterService: SequesterService
  ) { }

  ngOnInit(): void {
    this.getsInit();
  }

  ngAfterViewInit() {
    
  }

  applyFilter(event: Event) {
    this.filter.searchParam = (event.target as HTMLInputElement).value;
    this.gets();
  }

  getServerData(event:PageEvent){
    this.filter.currentPage = event?.pageIndex;
    this.filter.itemsPerPage = event?.pageSize;
    this.gets();
  }

  gets(){
    let filterJson = JSON.stringify(this.filter);
    this.sequesterService.gets(filterJson).subscribe(sequesters => {
      if (sequesters.isOK){
        this.dataSource.data = sequesters.resultObject.objects;
        setTimeout(() => {
          this.dataSource.paginator!.length = sequesters.resultObject.totalItems;
          this.dataSource.paginator!.pageIndex = sequesters.resultObject.currentPage;
          this.dataSource.paginator!.pageSize = sequesters.resultObject.itemsPerPage;
        }, 10);
      }
      
    });
  }

  getsInit(){
    console.log(1);
    let filterJson = JSON.stringify(this.filter);
    this.sequesterService.gets(filterJson).subscribe(sequesters => {
      console.log(sequesters);
      if (sequesters.isOK){
        this.dataSource = new MatTableDataSource(sequesters.resultObject.objects);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
      
    });
  }

  sortData(sort: Sort) {
    this.filter.sortingDirection = sort.direction;
    this.filter.sortingParam = sort.active;
    this.gets();
  }

  editUserDialog(user: SequesterDTO) {
    console.log(user);
  }

  deleteUserDialog(user: SequesterDTO) {
    // this.executorService.delete(Guid.parse(user.key)).subscribe(res => console.log(res));
  }

}
