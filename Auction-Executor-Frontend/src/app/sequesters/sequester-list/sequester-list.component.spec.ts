import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SequesterListComponent } from './sequester-list.component';

describe('SequesterListComponent', () => {
  let component: SequesterListComponent;
  let fixture: ComponentFixture<SequesterListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SequesterListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SequesterListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
