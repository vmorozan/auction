import { createAction, props } from "@ngrx/store";
import { NewCommonFilter } from "src/app/shared/models/common-filters/common-filters/new-common-fitler.model";

export const setCommonFilter = createAction(
    '[Sequester] Set Common Filter',
    props<{ commonFilter: NewCommonFilter[] }>()
);

export const setCategoryFilter = createAction(
    '[Sequester] Set Category Filter',
    props<{ categoryFilter: NewCommonFilter[] }>()
);

export const setSubCategoryFilter = createAction(
    '[Sequester] Set SubCategory Filter',
    props<{ subCategoryFilter: NewCommonFilter[] }>()
);

export const clearCommonFilter = createAction(
    '[Sequester] Clear Common Filter'
);

export const clearCategoryFilter = createAction(
    '[Sequester] Clear Category Filter'
);

export const clearSubCategoryFilter = createAction(
    '[Sequester] Clear SubCategory Filter'
);

export const initializeCommonFilter = createAction(
    '[Sequester] Initialize Common Filter'
);

export const initializeCategoryFilter = createAction(
    '[Sequester] Initialize Category Filter'
);

export const initializeSubCategoryFilter = createAction(
    '[Sequester] Initialize SubCategory Filter'
);