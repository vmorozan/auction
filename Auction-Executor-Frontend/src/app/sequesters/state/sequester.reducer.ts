import { createReducer, on } from "@ngrx/store";
import { NewCommonFilter } from "src/app/shared/models/common-filters/common-filters/new-common-fitler.model";
import * as AppState from '../../state/app.state';
import * as SequesterActions from '../state/sequester.actions';

export interface State extends AppState.State {
    sequesters: SequesterState;
}

export interface SequesterState {
    commonFilter: NewCommonFilter[];
    categoryFilter: NewCommonFilter[];
    subCategoryFilter: NewCommonFilter[];
}

const initialState: SequesterState = {
    commonFilter: [],
    categoryFilter: [],
    subCategoryFilter: []
}

export const sequesterReducer = createReducer<SequesterState>(
    initialState as SequesterState,
    on(SequesterActions.setCommonFilter, (state, action): SequesterState => {
        return {
            ...state,
            commonFilter: action.commonFilter
        }
    }),
    on(SequesterActions.setCategoryFilter, (state, action): SequesterState => {
        return {
            ...state,
            categoryFilter: action.categoryFilter
        }
    }),
    on(SequesterActions.setSubCategoryFilter, (state, action): SequesterState => {
        return {
            ...state,
            subCategoryFilter: action.subCategoryFilter
        }
    }),
    on(SequesterActions.clearCommonFilter, (state): SequesterState => {
        return {
            ...state,
            commonFilter: []
        }
    }),
    on(SequesterActions.clearCategoryFilter, (state): SequesterState => {
        return {
            ...state,
            categoryFilter: []
        }
    }),
    on(SequesterActions.clearSubCategoryFilter, (state): SequesterState => {
        return {
            ...state,
            subCategoryFilter: []
        }
    }),
    on(SequesterActions.initializeCommonFilter, (state): SequesterState => {
        return {
            ...state,
            commonFilter: []
        }
    }),
    on(SequesterActions.initializeCategoryFilter, (state): SequesterState => {
        return {
            ...state,
            categoryFilter: []
        }
    }),
    on(SequesterActions.initializeSubCategoryFilter, (state): SequesterState => {
        return {
            ...state,
            subCategoryFilter: []
        }
    }),
);