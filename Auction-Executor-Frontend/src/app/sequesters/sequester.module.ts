import { NgModule } from '@angular/core';
import { SequesterAddComponent } from './sequester-add/sequester-add.component';
import { MaterialModule } from '../material/material.module';
import { SequesterRoutingModule } from './sequester-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SequesterListComponent } from './sequester-list/sequester-list.component';
import { StoreModule } from '@ngrx/store';

@NgModule({
  declarations: [
    SequesterListComponent,
    SequesterAddComponent,
  ],
  imports: [
    MaterialModule,
    SequesterRoutingModule,
    FlexLayoutModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forFeature('sequester', {})
  ],
})
export class SequesterModule { }
