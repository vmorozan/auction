import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SequesterAddComponent } from './sequester-add/sequester-add.component';
import { SequesterListComponent } from './sequester-list/sequester-list.component';

const routes: Routes = [
    {
        path: 'list', component: SequesterListComponent
    },
    {
      path: 'add', component: SequesterAddComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SequesterRoutingModule { }
