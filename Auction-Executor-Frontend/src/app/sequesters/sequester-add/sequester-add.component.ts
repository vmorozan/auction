import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Guid } from 'guid-typescript';
import { CategoryListDTO } from 'src/app/shared/models/common-filters/categories/category-list-dto';
import { CommonFilterListDTO } from 'src/app/shared/models/common-filters/common-filters/common-filter-list-dto';
import { NewCommonFilter } from 'src/app/shared/models/common-filters/common-filters/new-common-fitler.model';
import { SubCategoryDTO } from 'src/app/shared/models/common-filters/sub-categories/sub-category-dto';
import { AuctionType } from 'src/app/shared/models/enums/auction-type.enum';
import { SequesterStatus } from 'src/app/shared/models/enums/sequester.enum';
import { SequesterDTO } from 'src/app/shared/models/sequesters/sequester.model';
import { FilterService } from 'src/app/shared/services/filter.service';
import { SequesterService } from 'src/app/shared/services/sequester.service';
import * as SequesterActions from '../state/sequester.actions';


@Component({
  selector: 'app-sequester-add',
  templateUrl: './sequester-add.component.html',
  styleUrls: ['./sequester-add.component.scss']
})
export class SequesterAddComponent implements OnInit {

  sequesterForm: FormGroup;
  categories!: CategoryListDTO[];
  subCategories!: SubCategoryDTO[];
  commonFilters: NewCommonFilter[] = [];
  categoryFilters: NewCommonFilter[] = [];
  subCategoryFilters: NewCommonFilter[] = [];
  takedFilters: CommonFilterListDTO[] = [];

  constructor(
    private sequesterService: SequesterService,
    private filterService: FilterService,
    private store: Store,
    private router:Router) { 
    this.sequesterForm = new FormGroup({
        name: new FormControl(null, [Validators.required]),
        description: new FormControl(null, [Validators.required]),
        markedPrice: new FormControl(null, [Validators.required]),
        dateExpired: new FormControl(null, [Validators.required]),
        categoryKey: new FormControl(null),
        subCategoryKey: new FormControl(null),
        file: new FormControl(''),
        fileSource: new FormControl(''),
        commonFilter: new FormArray([]),
        categoryFilter: new FormArray([]),
        subCategoryFilter: new FormArray([]),
    })
  }

  ngOnInit(): void {
    this.filterService.getCategories().subscribe(res => this.categories = res.resultObject);
    this.filterService.getListCommonFilters({} as CommonFilterListDTO).subscribe(filter => {
      filter.forEach(element => {
        if (this.commonFilters.length > 0){
          if (this.commonFilters.find(x => x.name === element.name)){
            this.commonFilters.find(x => x.name === element.name)?.filters.push({
              value: element.value,
              key: element.key
            });
          }
          else {
            this.commonFilters.push({
              name: element.name,
              filters: [{
                value: element.value,
                key: element.key
              }]
            });
          }
        }
        else if (this.commonFilters.length == 0){
          this.commonFilters.push({
            name: element.name,
            filters: [{
              value: element.value,
              key: element.key
            }]
          });
        }
      });

      this.store.dispatch(SequesterActions.setCommonFilter({ commonFilter: this.commonFilters }))
      this.commonFilters.forEach(() => {
        (<FormArray>this.sequesterForm.get('commonFilter')).push(new FormGroup({
          key: new FormControl(null)
        }));
      })
    });
  }

  getCategoryFilters(){
    this.categoryFilters = [];
    this.subCategoryFilters = [];
    this.filterService.getSubCategories(this.sequesterForm.get('categoryKey')?.value).subscribe(
      res => this.subCategories = res.resultObject
    )
    this.filterService.getListCommonFiltersByCategory(this.sequesterForm.get('categoryKey')?.value).subscribe(
      res => {
        res.forEach(element => {
          if (this.categoryFilters.length > 0){
            if (this.categoryFilters.find(x => x.name === element.name)){
              this.categoryFilters.find(x => x.name === element.name)?.filters.push({
                value: element.value,
                key: element.key
              });
            }
            else {
              this.categoryFilters.push({
                name: element.name,
                filters: [{
                  value: element.value,
                  key: element.key
                }]
              });
            }
          }
          else if (this.categoryFilters.length == 0){
            this.categoryFilters.push({
              name: element.name,
              filters: [{
                value: element.value,
                key: element.key
              }]
            });
          }
        })
        this.store.dispatch(SequesterActions.setCategoryFilter({ categoryFilter: this.categoryFilters }));
        if (res.length > 0){
          this.categoryFilters.forEach(() => {
            (<FormArray>this.sequesterForm.get('categoryFilter')).push(new FormGroup({
              key: new FormControl(null)
            }));
          })
        }
      }
    )
  }

  getSubCategoryFilters() {
    this.subCategoryFilters = [];
    this.filterService.getListCommonFiltersBySubCategoryKey(
      this.sequesterForm.get('categoryKey')?.value,
      this.sequesterForm.get('subCategoryKey')?.value).subscribe(
      res => {
        res.forEach(element => {
          if (this.subCategoryFilters.length > 0){
            if (this.subCategoryFilters.find(x => x.name === element.name)){
              this.subCategoryFilters.find(x => x.name === element.name)?.filters.push({
                value: element.value,
                key: element.key
              });
            }
            else {
              this.subCategoryFilters.push({
                name: element.name,
                filters: [{
                  value: element.value,
                  key: element.key
                }]
              });
            }
          }
          else if (this.subCategoryFilters.length == 0){
            this.subCategoryFilters.push({
              name: element.name,
              filters: [{
                value: element.value,
                key: element.key
              }]
            });
          }
        })

        this.store.dispatch(SequesterActions.setSubCategoryFilter({ subCategoryFilter: this.subCategoryFilters }));
        if (res.length > 0){
          this.subCategoryFilters.forEach(() => {
            (<FormArray>this.sequesterForm.get('subCategoryFilter')).push(new FormGroup({
              key: new FormControl(null)
            }));
          })
        }
      }
    )
  }

  submit(){
    let filters = [
      ...this.sequesterForm.get('commonFilter')?.value, 
      ...this.sequesterForm.get('categoryFilter')?.value, 
      ...this.sequesterForm.get('subCategoryFilter')?.value
    ];

    filters.forEach(element => {
      this.takedFilters.push(
        {
          key: element.key,
          subCategoryName: '',
          categoryName: '',
          name: '',
          value: '',
          id: 0
        }
      )
    })

    let sequester = {
      description: this.sequesterForm.get('description')?.value,
      name: this.sequesterForm.get('name')?.value,
      markedPrice: this.sequesterForm.get('markedPrice')?.value,
      file: this.sequesterForm.get('fileSource')?.value,
      dateExpired: this.sequesterForm.get('dateExpired')?.value,
      categoryKey: this.sequesterForm.get('categoryKey')?.value,
      subCategoryKey: this.sequesterForm.get('subCategoryKey')?.value,
      type: AuctionType.FirstAuction,
      status: SequesterStatus.InProgress,
      buyedPrice: 0,
      filters
    } as SequesterDTO

    console.log(sequester)
    this.sequesterService.create(sequester).subscribe(res => console.log(res));
  }

  onFileChange(event: any) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0] as File;
      this.sequesterForm.patchValue({
        fileSource: file
      });
    }
  }

}
