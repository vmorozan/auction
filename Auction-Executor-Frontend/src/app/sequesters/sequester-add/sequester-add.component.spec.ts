import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SequesterAddComponent } from './sequester-add.component';

describe('SequesterAddComponent', () => {
  let component: SequesterAddComponent;
  let fixture: ComponentFixture<SequesterAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SequesterAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SequesterAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
