import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from '../models/users/user';
import { map } from 'rxjs/operators'
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private baseURL = 'https://localhost:44357/api/';
  user = new User();
  private currentUserSource = new BehaviorSubject<User>(this.user);
  currentUser$ = this.currentUserSource.asObservable();

  constructor(private http: HttpClient, private router: Router) { }

  login(values: any){
    return this.http.post<User>(this.baseURL + 'account/login', values)
        .pipe(map(
          (user: User) => {
            if (user) {
              localStorage.setItem('token', user.token);
              this.currentUserSource.next(user);
            }
          }
        ));
    }

  register(values: any){
    return this.http.post<User>(this.baseURL + 'account/register', values)
        .pipe(map(
          (user: User) => {
            if (user) {
              localStorage.setItem('token', user.token);
            }
          }
        ));
  }

  logout(){
    localStorage.removeItem('token');
    this.currentUserSource.next(this.user);
    this.router.navigateByUrl('/');
  }
}
