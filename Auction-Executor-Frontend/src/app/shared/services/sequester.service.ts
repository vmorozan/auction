import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { ResultDTO } from '../models/DTOs/ResultDTO';
import { Result } from '../models/result';
import { SequesterDTO } from '../models/sequesters/sequester.model';

@Injectable({
  providedIn: 'root'
})
export class SequesterService {
  private baseURL = environment.urlApi;

  constructor(private http: HttpClient, private router: Router) { }

  gets(filter: string){
    return this.http.get<Result<ResultDTO<SequesterDTO>>>(`${this.baseURL}sequester/gets?filter=${filter}`);
  }

  create(sequester: SequesterDTO){
    return this.http.post<Result<SequesterDTO>>(`${this.baseURL}sequester/create`, sequester);
  }
}
