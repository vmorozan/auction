import { NewCommonFilterList } from "./new-common-filter-list.model";

export class NewCommonFilter {
    name!: string;
    filters!: NewCommonFilterList[];
}