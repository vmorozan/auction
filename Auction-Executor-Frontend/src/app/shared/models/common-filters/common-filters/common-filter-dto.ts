import { Base } from "../../base";

export class CommonFilterDTO extends Base {
    categoryKey!: string;
    subCategoryKey!: string;
    name!: string;
    value!: string;
}