import { Guid } from "guid-typescript";

export class ExecutorList {
    key: string;
    firstName: string;
    lastName: string;
    city: string;
    executorNumber: string;
    street: string;

    constructor() {
        this.key = '';
        this.firstName = '';
        this.lastName = '';
        this.city = '';
        this.executorNumber = '';
        this.street = '';
    }
}