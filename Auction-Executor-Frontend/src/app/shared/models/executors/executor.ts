export class Executor {
    key: string;
    userName: string;
    email: string;
    firstName: string;
    lastName: string;
    city: string;
    zipCode: string;
    street: string;
    executorNumber: string;

    constructor() {
        this.key = '';
        this.userName = '';
        this.email = '';
        this.firstName = '';
        this.lastName = '';
        this.city = '';
        this.zipCode = '';
        this.street = '';
        this.executorNumber = '';
    }
}