export enum AuctionType {
    FirstAuction = 1,
    SecondAuction = 2,
    ThirdAuction = 3
}