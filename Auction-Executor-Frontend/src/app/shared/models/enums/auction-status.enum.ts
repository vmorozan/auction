export enum AuctionStatus {
    InProgress = 1,
    Expired = 2
}