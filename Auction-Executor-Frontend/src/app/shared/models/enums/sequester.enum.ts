export enum SequesterStatus
{
    InProgress = 1,
    Expired = 2
}