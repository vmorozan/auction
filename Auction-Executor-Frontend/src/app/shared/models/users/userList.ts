export class UserList {
    key: string;
    userName: string;
    email: string;
    firstName: string;
    lastName: string;
    zipCode: string;
    city: string;

    constructor() {
        this.key = '';
        this.userName = '';
        this.email = '';
        this.firstName = '';
        this.lastName = '';
        this.zipCode = '';
        this.city = '';
    }
}
