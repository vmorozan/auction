export class Result<T> {
    resultCode: ResultCode;
    resultMessage: string;
    isOk: boolean;
    resultObject!: T;

    public get isOK() {
        return this.resultCode == ResultCode.SUCCESS;
    }

    constructor() {
        this.resultCode = ResultCode.SUCCESS;
        this.resultMessage = '';
        this.isOk = true;
    }
}


enum ResultCode {
    SUCCESS = 0,
    FAILED = 1
}