import { CommonFilterDTO } from "../common-filters/common-filters/common-filter-dto";
import { AuctionType } from "../enums/auction-type.enum";
import { SequesterStatus } from "../enums/sequester.enum";

export class SequesterDTO {
    description!: string;
    status!: SequesterStatus;
    type!: AuctionType;
    name!: string;
    categoryKey!: string;
    subCategoryKey!: string;
    dateExpired!: Date;
    markedPrice!: number;
    buyedPrice!: number;
    file!: File;
    filters!: CommonFilterDTO[];
}