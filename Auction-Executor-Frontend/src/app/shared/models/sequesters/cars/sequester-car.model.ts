import { Base } from "../../base";
import { AuctionStatus } from "../../enums/auction-status.enum";
import { AuctionType } from "../../enums/auction-type.enum";

export class SequesterCarDTO extends Base {
    creator!: string;
    category!: string;
    status!: number;
    type!: AuctionType;
    model!: string;
    mark!: string;
    matriculate!: string;
    state!: AuctionStatus;
    comments!: string;
    region!: string;
    wheel!: string;
    price!: number;
}