using Auction.API.Helpers;
using Auction.Application.Extensions;
using Auction.Application.Helpers;
using Auction.Application.Interfaces;
using Auction.Application.Interfaces.Account;
using Auction.Application.Interfaces.Executors;
using Auction.Application.Interfaces.Files;
using Auction.Application.Interfaces.Filters;
using Auction.Application.Interfaces.Sequesters;
using Auction.Application.Interfaces.Tokens;
using Auction.Application.Interfaces.Users;
using Auction.Application.Paginations.Executors;
using Auction.Application.Paginations.Filters;
using Auction.Application.Paginations.Interfaces.Executors;
using Auction.Application.Paginations.Interfaces.Filters;
using Auction.Application.Paginations.Interfaces.Sequesters;
using Auction.Application.Paginations.Interfaces.Users;
using Auction.Application.Paginations.Sequesters;
using Auction.Application.Paginations.Users;
using Auction.Application.Services.Account;
using Auction.Application.Services.Executors;
using Auction.Application.Services.Files;
using Auction.Application.Services.Filters;
using Auction.Application.Services.Sequesters;
using Auction.Application.Services.Tokens;
using Auction.Application.Services.Users;
using Auction.Data;
using Auction.Domain.Users;
using Auction.Infrastructure.Interfaces;
using Auction.Infrastructure.Repositories;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System.Linq;

namespace Auction
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Auction", Version = "v1" });
                c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
                c.OperationFilter<SwaggerFileOperationFilter>();
            });

            services.AddScoped<IUserPaginator, UserPaginator>();
            services.AddScoped<IAccountRepository, AccountRepository>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IExecutorService, ExecutorService>();
            services.AddScoped<IExecutorRepository, ExecutorRepository>();
            services.AddScoped<IFileService, FileService>();
            services.AddScoped<IFileRepository, FileRepository>();
            services.AddScoped<IExecutorPaginator, ExecutorPaginator>();
            services.AddScoped<ICommonFilterPaginator, CommonFilterPaginator>();
            services.AddScoped<ICategoryPaginator, CategoryPaginator>();
            services.AddScoped<ISubCategoryPaginator, SubCategoryPaginator>();
            services.AddScoped<ISequesterService, SequesterService>();
            services.AddScoped<ISequesterRepository, SequesterRepository>();
            services.AddScoped<ISequesterPaginator, SequesterPaginator>();
            services.AddScoped<IFilterRepository, FilterRepository>();
            services.AddScoped<IFilterService, FilterService>();
            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<UserManager<User>, UserManager<User>>();
            services.AddAutoMapper(typeof(MappingProfiles));
            services.AddIdentityService(Configuration);
            services.AddDbContext<AuctionDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"),
                    assembly => assembly.MigrationsAssembly(typeof(AuctionDbContext).Assembly.FullName)));

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie();

            services.AddCors(opt =>
            {
                opt.AddPolicy("CorsPolicy", policy =>
                {
                    policy.AllowAnyHeader().AllowAnyMethod().WithOrigins("http://localhost:4200");
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Auction v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseStaticFiles();

            app.UseCors("CorsPolicy");


            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
