﻿using Auction.Application.DTOs.Users;
using Auction.Application.Helpers.Models;
using Auction.Application.Interfaces.Users;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Auction.Controllers
{
    public class UserController : BaseApiController
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet("gets")]
        public async Task<ActionResult<ResultListDTO<UserListDTO>>> Gets([FromQuery]string filter)
        {
            return Ok(await _userService.Gets(filter));
        }

        [HttpDelete("delete")]
        public async Task<ActionResult<bool>> Delete([FromQuery] Guid key)
        {
            return Ok(await _userService.Delete(key));
        }
    }
}
