﻿using Auction.Application.DTOs.Common;
using Auction.Application.DTOs.Executor;
using Auction.Application.Helpers.Models;
using Auction.Application.Interfaces.Executors;
using Auction.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auction.API.Controllers
{
    public class ExecutorController : BaseApiController
    {
        private readonly IExecutorService _executorService;

        public ExecutorController(IExecutorService executorService)
        {
            _executorService = executorService;
        }

        [HttpGet("gets")]
        public async Task<ActionResult<Result<ResultListDTO<ExecutorListDTO>>>> Gets([FromQuery]string filter)
        {
            return Ok(await _executorService.Gets(filter));
        }

        [HttpDelete("delete")]
        public async Task<ActionResult<Result<bool>>> Delete([FromQuery] Guid key)
        {
            return Ok(await _executorService.Delete(key));
        }

        [HttpPost("add")]
        public async Task<ActionResult<Result<bool>>> Add([FromBody] ExecutorDTO executor)
        {
            return Ok(await _executorService.Add(executor));
        }

        [HttpPut("update")]
        public async Task<ActionResult<Result<ExecutorDTO>>> Update([FromBody] ExecutorDTO executor)
        {
            return Ok(await _executorService.Update(executor));
        }

    }
}
