﻿using Auction.Application.DTOs.Files;
using Auction.Application.Interfaces.Files;
using Auction.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Auction.API.Controllers
{
    public class FilesController : BaseApiController
    {
        private readonly IFileService _fileService;

        public FilesController(IFileService fileService)
        {
            _fileService = fileService;
        }

        [HttpGet("preview")]
        public async Task<ActionResult> Preview([FromQuery] Guid Key)
        {
            var res = await _fileService.Preview(Key);
            return File(res.FileType, res.MimeType, res.Name);
        }

        [HttpPost("upload")]
        public async Task<ActionResult<FileDTO>> Upload([FromForm] IFormFile file)
        {
            return Ok(await _fileService.Upload(new UploadFileDTO() {
                File = file,
                OrderedId = 1
            }));
        }
    }
}
