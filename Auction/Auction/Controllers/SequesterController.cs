﻿using Auction.Application.DTOs.Common;
using Auction.Application.DTOs.Sequesters;
using Auction.Application.Helpers.Models;
using Auction.Application.Interfaces.Sequesters;
using Auction.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auction.API.Controllers
{
    public class SequesterController : BaseApiController
    {
        private readonly ILogger<SequesterController> _logger;
        private readonly ISequesterService _sequesterService;

        public SequesterController(ILogger<SequesterController> logger, ISequesterService sequesterService)
        {
            _logger = logger;
            _sequesterService = sequesterService;
        }

        [HttpGet("gets")]
        public async Task<ActionResult<Result<ResultListDTO<SequesterDTO>>>> Gets([FromQuery] string filter)
        {
            return Ok(await _sequesterService.Gets(filter));
        }

        [HttpGet]
        public async Task<ActionResult<Result<ResultListDTO<SequesterDTO>>>> Get([FromQuery]Guid SequesterKey)
        {
            return Ok(await _sequesterService.Get(SequesterKey));
        }

        [HttpPost("create")]
        public async Task<ActionResult<Result<SequesterDTO>>> Create([FromBody] SequesterDTO sequester)
        {
            return Ok(await _sequesterService.Create(sequester));
        }

        [HttpPost("upload-file")]
        public async Task<ActionResult<Result<SequesterDTO>>> AddFilters([FromBody] SequesterDTO sequester)
        {
            var result = await _sequesterService.AddFilters(sequester);
            return Ok(result);
        }
     }
}
