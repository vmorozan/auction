﻿using Auction.Application.DTOs.Common;
using Auction.Application.DTOs.Filters.Categories;
using Auction.Application.DTOs.Filters.Commons;
using Auction.Application.Helpers.Models;
using Auction.Application.Interfaces.Filters;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Auction.Controllers
{
    public class FilterController : BaseApiController
    {
        private readonly ILogger<FilterController> _logger;
        private readonly IFilterService _filterService;

        public FilterController(ILogger<FilterController> logger, IFilterService filterService)
        {
            _logger = logger;
            _filterService = filterService;
        }

        [HttpGet("categories/gets")]
        public async Task<ActionResult<Result<IReadOnlyList<CategoryDTO>>>> GetCategories()
        {
            return Ok(await _filterService.GetCategories());
        }

        [HttpGet("list-categories/gets")]
        public async Task<ActionResult<Result<ResultListDTO<CategoryDTO>>>> GetListCategories([FromQuery] string filter)
        {
            return Ok(await _filterService.GetListCategories(filter));
        }

        [HttpGet("sub-categories/gets")]
        public async Task<ActionResult<Result<IReadOnlyList<SubCategoryDTO>>>> GetSubCategories([FromQuery]Guid categoryKey)
        {
            return Ok(await _filterService.GetSubCategories(categoryKey));
        }

        [HttpGet("list-sub-categories/gets")]
        public async Task<ActionResult<Result<IReadOnlyList<SubCategoryDTO>>>> GetListSubCategories([FromQuery] string filter)
        {
            return Ok(await _filterService.GetListSubCategories(filter));
        }

        [HttpPost("category/add")]
        public async Task<ActionResult<CategoryDTO>> AddCategory([FromBody]CategoryDTO category)
        {
            return Ok(await _filterService.AddCategory(category));
        }


        [HttpPost("sub-category/add")]
        public async Task<ActionResult<CategoryDTO>> AddSubCategory([FromBody] SubCategoryDTO subCategory)
        {
            return Ok(await _filterService.AddSubCategory(subCategory));
        }

        [HttpPost("common-filter/add")]
        public async Task<ActionResult<CommonFilterDTO>> AddCommonFilter([FromBody] CommonFilterDTO commonFilter)
        {
            return Ok(await _filterService.AddCommonFilter(commonFilter));
        }

        [HttpGet("common-filter/gets")]
        public async Task<ActionResult<Result<ResultListDTO<CommonFilterDTO>>>> GetsCommonFilter([FromQuery] string commonFilter)
        {
            return Ok(await _filterService.GetsCommonFilter(commonFilter));
        }

        [HttpPost("list-common-filter/gets")]
        public async Task<ActionResult<IReadOnlyList<CommonFilterDTO>>> GetListCommonFilter([FromBody] CommonFilterDTO filter)
        {
            return Ok(await _filterService.GetListCommonFilter(filter));
        }

        [HttpGet("list-common-filter-by-category/gets")]
        public async Task<ActionResult<IReadOnlyList<CommonFilterDTO>>> GetListCommonFilterByCategory([FromQuery] Guid categoryKey)
        {
            return Ok(await _filterService.GetListCommonFilterByCategory(categoryKey));
        }

        [HttpGet("list-common-filter-by-sub-category/gets")]
        public async Task<ActionResult<IReadOnlyList<CommonFilterDTO>>> GetListCommonFilterBySubCategory([FromQuery] Guid categoryKey, [FromQuery] Guid subCategoryKey)
        {
            return Ok(await _filterService.GetListCommonFilterBySubCategory(categoryKey, subCategoryKey));
        }

        [HttpGet("city/gets")]
        public async Task<ActionResult<IReadOnlyList<SubCategoryDTO>>> GetCities()
        {
            return Ok(await _filterService.GetCities());
        }
    }
}
