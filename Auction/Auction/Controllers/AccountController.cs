﻿using Auction.Application.DTOs.Users;
using Auction.Application.Interfaces.Account;
using Auction.Application.Interfaces.Tokens;
using Auction.Domain.Persons;
using Auction.Domain.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Auction.Controllers
{
    public class AccountController : BaseApiController
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly ITokenService _tokenService;
        private readonly IAccountService _accountService;
        public AccountController(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            ITokenService tokenService,
            IAccountService accountService)
        {
            _userManager = userManager;
            _tokenService = tokenService;
            _signInManager = signInManager;
            _accountService = accountService;
        }

        [HttpGet("GetCurrentUser")]
        public async Task<ActionResult<UserDTO>> GetCurrentUser()
        {
            var user = await _userManager.GetUserAsync(User);

            if (user == null) return null;

            return new UserDTO
            {
                Id = Int32.Parse(user.Id.ToString()),
                Email = user.Email,
                Token = _tokenService.CreateToken(user),
            };
        }

        [HttpPost("Login")]
        public async Task<ActionResult<UserDTO>> Login([FromBody] LoginDTO loginDto)
        {
            var user = await _userManager.FindByEmailAsync(loginDto.Email);

            if (user == null) return null;

            var result = await _signInManager.CheckPasswordSignInAsync(user, loginDto.Password, false);

            if (!result.Succeeded) return null;

            return new UserDTO
            {
                Email = user.Email,
                Token = _tokenService.CreateToken(user),
                UserName = user.UserName,
                Key = user.Key
            };
        }

        [HttpPost("Register")]
        public async Task<ActionResult<UserDTO>> Register([FromBody] RegisterDTO registerDto)
        {
            return Ok(await _accountService.Register(registerDto));
        }
    }
}
