﻿using Auction.Domain.Executors;
using System;
using System.ComponentModel.DataAnnotations;

namespace Auction.Domain.Files
{
    public class FileEntity : BaseEntity
    {
        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(100)]
        public string MimeType { get; set; }

        public int OrderedId { get; set; }

        [MaxLength]
        public byte[] FileType { get; set; }

        public DateTime? Created { get; set; }

        public Guid? SequesterKey { get; set; }

        #nullable enable
        public Sequester? Sequester { get; set; }

        public Guid? ExecutorKey { get; set; }

        #nullable enable
        public Executor? Executor { get; set; }
    }
}
