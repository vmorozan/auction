﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Domain.Others
{
    public enum AuctionType
    {
        FirstAuction = 1,
        SecondAuction = 2,
        ThirdAuction = 3
    }
}
