﻿namespace Auction.Domain.Others
{
    public enum UserType
    {
        Admin = 0,
        Executor,
        SimpleUser
    }
}
