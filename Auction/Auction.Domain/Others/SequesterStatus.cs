﻿namespace Auction.Domain.Others
{
    public enum SequesterStatus
    {
        InProgress = 1,
        Expired = 2
    }
}
