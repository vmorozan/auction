﻿using Auction.Domain.Addresses;
using Auction.Domain.Filters;
using Auction.Domain.Others;
using Auction.Domain.Users;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Auction.Domain.RealEstates
{
    public class AdvertismentRealEstate : BaseEntity
    {
        [ForeignKey("Creator")]
        public Guid CreatorKey { get; set; }

        public User Creator { get; set; }

        [ForeignKey("Category")]
        public Guid CategoryKey { get; set; }

        public Category Category { get; set; }

        [ForeignKey("SubCategory")]
        public Guid SubCategoryKey { get; set; }

        public SubCategory SubCategory { get; set; }

        [ForeignKey("AuthorAnnouncement")]
        public Guid AuthorAnnouncementKey { get; set; }

        public CommonFilters AuthorAnnouncement { get; set; }

        [ForeignKey("HousingFond")]
        public Guid HousingFondKey { get; set; }

        public CommonFilters HousingFond { get; set; }

        [ForeignKey("TypeBuilding")]
        public Guid TypeBuildingKey { get; set; }

        public CommonFilters TypeBuilding { get; set; }

        [ForeignKey("Sequester")]
        public Guid SequesterKey { get; set; }

        public Sequester Sequester { get; set; }
    }
}
