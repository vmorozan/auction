﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Domain.Filters
{
    public class SubCategory : BaseEntity
    {
        public string Name { get; set; }

        [ForeignKey("Category")]
        public Guid CategoryKey { get; set; }

        public Category Category { get; set; }
    }
}
