﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Domain.Filters
{
    public class CommonFiltersSequester
    {
        [Key]
        public Guid Key { get; set; }

        [ForeignKey("Sequester")]
        public Guid SequesterKey { get; set; }

        public Sequester Sequester { get; set; }

        [ForeignKey("Filter")]
        public Guid FilterKey { get; set; }

        public CommonFilters Filter { get; set; }
    }
}
