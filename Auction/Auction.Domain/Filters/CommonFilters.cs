﻿using Auction.Domain.Cars;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Domain.Filters
{
    public class CommonFilters : BaseEntity
    {
        [ForeignKey("Category")]
        public Guid? CategoryKey { get; set; }

        public Category Category { get; set; }

        [ForeignKey("SubCategory")]
        public Guid? SubCategoryKey { get; set; }

        public SubCategory SubCategory { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

        public ICollection<CommonFiltersSequester> Sequester { get; set; }
    }
}
