﻿
namespace Auction.Domain.Filters
{
    public class EntityFilter
    {
        public string SortingParam { get; set; }

        public string SortingDirection { get; set; }

        public string SearchParam { get; set; }

        public int CurrentPage { get; set; }

        public int ItemsPerPage { get; set; }
    }
}
