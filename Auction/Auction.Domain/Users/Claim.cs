﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Auction.Domain.Users
{
    public class Claim : BaseEntity
    {
        public string ClaimValue { get; set; }

        public string ClaimType { get; set; }

        [ForeignKey("User")]
        public Guid UserKey { get; set; }

        public User User { get; set; }
    }
}
