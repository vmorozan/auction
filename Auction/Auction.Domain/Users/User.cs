﻿using Auction.Domain.Executors;
using Auction.Domain.Others;
using Auction.Domain.Persons;
using Auction.Domain.Roles;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Auction.Domain.Users
{
    public class User : IdentityUser
    {
        [Key]
        public Guid Key { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public string Password { get; set; }

        [ForeignKey("Person")]
        public Guid? PersonKey { get; set; }

        public Person Person { get; set; }

        public DateTime DateRegistration { get; set; }

        public UserType UserType { get; set; }

        public ICollection<Role> Roles { get; set; }

        public ICollection<Claim> Claims { get; set; }

        [ForeignKey("ExecutorDetails")]
        public Guid? ExecutorDetailsKey { get; set; }

        public Executor ExecutorDetails { get; set; }

        public bool IsDeleted { get; set; }
    }
}
