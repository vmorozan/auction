﻿using Auction.Domain.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Domain.Roles
{
    public class Role : BaseEntity
    {
        public string Name { get; set; }

        [ForeignKey("User")]
        public Guid? UserKey { get; set; }

        public User User { get; set; }

        public ICollection<Claim> Claims { get; set; }
    }
}
