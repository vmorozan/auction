﻿using Auction.Domain.Addresses;
using Auction.Domain.Files;
using Auction.Domain.Filters;
using Auction.Domain.Others;
using Auction.Domain.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Domain
{
    public class Sequester : BaseEntity
    {
        public string Description { get; set; }

        public string Name { get; set; }

        public SequesterStatus Status { get; set; }

        public AuctionType Type { set; get; }

        public DateTime DateRegistration { get; set; }

        public DateTime DateExpired { get; set; }

        public string Comments { get; set; }

        [ForeignKey("Creator")]
        public Guid CreatorKey { get; set; }

        public User Creator { get; set; }

        [ForeignKey("Category")]
        public Guid CategoryKey { get; set; }

        public Category Category { get; set; }

        [ForeignKey("SubCategory")]
        public Guid SubCategoryKey { get; set; }

        public SubCategory SubCategory { get; set; }

        [Column(TypeName = "decimal(12,2)")]
        public decimal MarkedPrice { get; set; }

        [Column(TypeName = "decimal(12,2)")]
        public decimal? BuyedPrice { get; set; }

        public ICollection<CommonFiltersSequester> Filters { get; set; }

        public ICollection<FileEntity> Files { get; set; }
    }
}
