﻿using Auction.Domain.Addresses;
using Auction.Domain.Others;
using System;

namespace Auction.Domain.Persons
{
    public class Person : BaseEntity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Guid? AddressKey { get; set; }

        public Address Address { get; set; }
    }
}
