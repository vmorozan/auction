﻿using Auction.Domain.Files;
using Auction.Domain.Others;
using System;
using System.Collections.Generic;

namespace Auction.Domain.Executors
{
    public class Executor : BaseEntity
    {
        public string ExecutorNumber { get; set; }

        public ICollection<FileEntity> Files { get; set; }
    }
}
