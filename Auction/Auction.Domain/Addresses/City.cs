﻿namespace Auction.Domain.Addresses
{
    public class City : BaseEntity
    {
        public string Name { get; set; }
    }
}
