﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Domain.Addresses
{
    public class Region : BaseEntity
    {
        public string Name { get; set; }
    }
}
