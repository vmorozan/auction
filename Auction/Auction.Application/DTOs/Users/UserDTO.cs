﻿namespace Auction.Application.DTOs.Users
{
    public class UserDTO : BaseDTO
    {
        public string Email { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string City { get; set; }

        public string Street { get; set; }

        public string Token { get; set; }
    }
}
