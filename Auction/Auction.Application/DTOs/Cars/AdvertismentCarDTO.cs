﻿using Auction.Domain.Others;
using System;

namespace Auction.Application.DTOs.Cars
{
    public class AdvertismentCarDTO
    {
        public string Name { get; set; }

        public Guid CreatorKey { get; set; }

        public Guid CategoryKey { get; set; }

        public Guid SubCategoryKey { get; set; }

        public SequesterStatus Status { get; set; }

        public AuctionType Type { set; get; }

        public Guid ModelKey { get; set; }

        public Guid MarkKey { get; set; }

        public Guid MatriculateKey { get; set; }

        public Guid StateKey { get; set; }

        public string Comments { get; set; }

        public Guid RegionKey { get; set; }

        public Guid WheelKey { get; set; }

        public double Price { get; set; }
    }
}
