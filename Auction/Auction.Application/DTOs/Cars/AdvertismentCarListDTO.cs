﻿using Auction.Domain.Others;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Application.DTOs.Cars
{
    public class AdvertismentCarListDTO
    {
        public string Creator { get; set; }

        public string Category { get; set; }

        public string SubCategory { get; set; }

        public SequesterStatus Status { get; set; }

        public AuctionType Type { set; get; }

        public string Model { get; set; }

        public string Mark { get; set; }

        public string Matriculate { get; set; }

        public string State { get; set; }

        public string Comments { get; set; }

        public string Region { get; set; }

        public string Wheel { get; set; }

        public double Price { get; set; }
    }
}
