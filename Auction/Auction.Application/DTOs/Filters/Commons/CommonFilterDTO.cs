﻿using System;

namespace Auction.Application.DTOs.Filters.Commons
{
    public class CommonFilterDTO : BaseDTO
    {
        public Guid CategoryKey { get; set; }

        public string CategoryName { get; set; }

        public Guid SubCategoryKey { get; set; }

        public string SubCategoryName { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }
    }
}
