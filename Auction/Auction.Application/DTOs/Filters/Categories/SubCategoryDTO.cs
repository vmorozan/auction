﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Application.DTOs.Filters.Categories
{
    public class SubCategoryDTO : BaseDTO
    {
        public Guid CategoryKey { get; set; }

        public string CategoryName { get; set; }

        public string Name { get; set; }
    }
}
