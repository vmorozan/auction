﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Application.DTOs.Filters.Categories
{
    public class CategoryDTO : BaseDTO
    {
        public string Name { get; set; }
    }
}
