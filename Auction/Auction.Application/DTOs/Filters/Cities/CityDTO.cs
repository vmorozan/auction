﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Application.DTOs.Filters.Cities
{
    public class CityDTO : BaseDTO
    {
        public string Name { get; set; }
    }
}
