﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Application.DTOs.Common
{
    public enum ResultCode
    {
        SUCCESS = 0,
        FAILED = 1
    }
}
