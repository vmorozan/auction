﻿using System.Collections.Generic;

namespace Auction.Application.Helpers.Models
{
    public class ResultListDTO<T> where T: class
    {

        public List<T> Objects { get; set; }

        public int TotalItems { get; set; }

        public int CurrentPage { get; set; }

        public int ItemPerPage { get; set; }
    }
}
