﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Application.DTOs.Common
{
    public class Result<T>
    {
        public ResultCode ResultCode { get; set; }

        public string ResultMessage { get; set; }

        public T ResultObject { get; set; }

        public bool IsOK { get { return ResultCode == ResultCode.SUCCESS; } }
    }
}
