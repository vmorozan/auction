﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Application.DTOs.Files
{
    public class UploadFileDTO
    {
        public IFormFile File { get; set; }

        public int OrderedId { get; set; }

        public Guid? SequesterKey { get; set; }

        public Guid? ExecutorKey { get; set; }
    }
}
