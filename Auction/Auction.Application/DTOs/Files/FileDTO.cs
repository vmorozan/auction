﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Application.DTOs.Files
{
    public class FileDTO
    {
        public string Name { get; set; }

        public string MimeType { get; set; }

        public byte[] FileType { get; set; }
    }
}
