﻿using Auction.Application.DTOs.Files;
using Auction.Application.DTOs.Filters.Commons;
using Auction.Domain.Others;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Application.DTOs.Sequesters
{
    public class SequesterDTO
    {
        public string Description { get; set; }

        public SequesterStatus Status { get; set; }

        public AuctionType Type { set; get; }

        public string Name { get; set; }

        public Guid CategoryKey { get; set; }

        public Guid SubCategoryKey { get; set; }

        public string Comments { get; set; }

        public decimal MarkedPrice { get; set; }

        public decimal BuyedPrice { get; set; }

        public FileDTO File { get; set; }

        public List<CommonFilterDTO> Filters { get; set; }
    }
}
