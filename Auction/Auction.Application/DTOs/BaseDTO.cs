﻿using System;

namespace Auction.Application.DTOs
{
    public class BaseDTO
    {
        public int Id { get; set; }
        public Guid Key { get; set; }
    }
}
