﻿using Auction.Application.DTOs.Cars;
using Auction.Application.DTOs.Executor;
using Auction.Application.DTOs.Files;
using Auction.Application.DTOs.Filters.Categories;
using Auction.Application.DTOs.Filters.Cities;
using Auction.Application.DTOs.Filters.Commons;
using Auction.Application.DTOs.Sequesters;
using Auction.Application.DTOs.Users;
using Auction.Domain;
using Auction.Domain.Addresses;
using Auction.Domain.Cars;
using Auction.Domain.Files;
using Auction.Domain.Filters;
using Auction.Domain.Users;
using AutoMapper;

namespace Auction.Application.Helpers
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<Category, CategoryDTO>();
            CreateMap<SubCategory, SubCategoryDTO>();
            CreateMap<User, UserListDTO>()
                .ForMember(x => x.FirstName, o => o.MapFrom(p => p.Person.FirstName))
                .ForMember(x => x.LastName, o => o.MapFrom(p => p.Person.LastName))
                .ForMember(x => x.City, o => o.MapFrom(p => p.Person.Address.City))
                .ForMember(x => x.ZipCode, o => o.MapFrom(p => p.Person.Address.ZipCode));
            CreateMap<CategoryDTO, Category>();
            CreateMap<City, CityDTO>();
            CreateMap<SubCategoryDTO, SubCategory>();
            CreateMap<CommonFilterDTO, CommonFilters>();
            CreateMap<User, ExecutorListDTO>()
                .ForMember(x => x.FirstName, o => o.MapFrom(p => p.Person.FirstName))
                .ForMember(x => x.LastName, o => o.MapFrom(p => p.Person.LastName))
                .ForMember(x => x.City, o => o.MapFrom(p => p.Person.Address.City))
                .ForMember(x => x.Street, o => o.MapFrom(p => p.Person.Address.Street))
                .ForMember(x => x.ExecutorNumber, o => o.MapFrom(p => p.ExecutorDetails.ExecutorNumber));
            CreateMap<CommonFilters, CommonFilterDTO>()
                .ForMember(x => x.CategoryName, o => o.MapFrom(p => p.Category.Name))
                .ForMember(x => x.SubCategoryName, o => o.MapFrom(p => p.SubCategory.Name));
            CreateMap<FileEntity, FileDTO>();
            CreateMap<Sequester, SequesterDTO>();
            CreateMap<CommonFiltersSequester, CommonFilterDTO>()
                .ForMember(d => d.Key, o => o.MapFrom(p => p.FilterKey))
                .ForMember(d => d.Name, o => o.MapFrom(p => p.Filter.Name))
                .ForMember(d => d.Value, o => o.MapFrom(p => p.Filter.Value))
                .ForMember(d => d.CategoryName, o => o.MapFrom(p => p.Filter.Category.Name))
                .ForMember(d => d.SubCategoryName, o => o.MapFrom(p => p.Filter.SubCategory.Name));
            CreateMap<SequesterDTO, Sequester>()
                .ForMember(x => x.Filters, o => o.Ignore());
        }
    }
}
