﻿using Auction.Application.DTOs.Common;
using Auction.Application.DTOs.Executor;
using Auction.Application.Helpers.Models;
using System;
using System.Threading.Tasks;

namespace Auction.Application.Interfaces.Executors
{
    public interface IExecutorService
    {
        Task<Result<ResultListDTO<ExecutorListDTO>>> Gets(string filter);

        Task<Result<bool>> Delete(Guid key);

        Task<Result<bool>> Add(ExecutorDTO executor);

        Task<Result<ExecutorDTO>> Update(ExecutorDTO executor);
    }
}
