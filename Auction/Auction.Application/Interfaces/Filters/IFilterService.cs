﻿using Auction.Application.DTOs.Common;
using Auction.Application.DTOs.Filters.Categories;
using Auction.Application.DTOs.Filters.Cities;
using Auction.Application.DTOs.Filters.Commons;
using Auction.Application.Helpers.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Auction.Application.Interfaces.Filters
{
    public interface IFilterService
    {
        Task<Result<IReadOnlyList<CategoryDTO>>> GetCategories();

        Task<Result<ResultListDTO<CategoryDTO>>> GetListCategories(string filter);

        Task<Result<IReadOnlyList<SubCategoryDTO>>> GetSubCategories(Guid categoryKey);

        Task<Result<ResultListDTO<SubCategoryDTO>>> GetListSubCategories(string filter);

        Task<IReadOnlyList<CityDTO>> GetCities();

        Task<CategoryDTO> GetCategoryByName(string name);

        Task<CategoryDTO> AddCategory(CategoryDTO category);

        Task<SubCategoryDTO> AddSubCategory(SubCategoryDTO category);

        Task<CommonFilterDTO> AddCommonFilter(CommonFilterDTO category);

        Task<IReadOnlyList<CommonFilterDTO>> GetListCommonFilter(CommonFilterDTO filter);

        Task<IReadOnlyList<CommonFilterDTO>> GetListCommonFilterByCategory(Guid categoryKey);

        Task<IReadOnlyList<CommonFilterDTO>> GetListCommonFilterBySubCategory(Guid categoryKey, Guid subCategoryKey);

        Task<Result<ResultListDTO<CommonFilterDTO>>> GetsCommonFilter(string filter);
    }
}
