﻿using Auction.Application.DTOs.Users;
using Auction.Application.Helpers.Models;
using System;
using System.Threading.Tasks;

namespace Auction.Application.Interfaces.Users
{
    public interface IUserService
    {
        Task<ResultListDTO<UserListDTO>> Gets(string filter);

        Task<bool> Delete(Guid key);
    }
}
