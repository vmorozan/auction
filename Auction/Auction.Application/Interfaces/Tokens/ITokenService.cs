﻿using Auction.Domain.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Application.Interfaces.Tokens
{
    public interface ITokenService
    {
        string CreateToken(User user);
    }
}
