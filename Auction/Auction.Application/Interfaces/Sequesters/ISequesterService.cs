﻿using Auction.Application.DTOs.Common;
using Auction.Application.DTOs.Sequesters;
using Auction.Application.Helpers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Application.Interfaces.Sequesters
{
    public interface ISequesterService
    {
        Task<Result<ResultListDTO<SequesterDTO>>> Gets(string filter);

        Task<Result<SequesterDTO>> Get(Guid sequesterKey);

        Task<Result<SequesterDTO>> Create(SequesterDTO sequester);

        Task<Result<SequesterDTO>> AddFilters(SequesterDTO sequester);
    }
}
