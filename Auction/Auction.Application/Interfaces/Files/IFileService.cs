﻿using Auction.Application.DTOs.Files;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Application.Interfaces.Files
{
    public interface IFileService
    {
        Task<FileDTO> Preview(Guid sequesterKey);

        Task<FileDTO> Upload(UploadFileDTO file);
    }
}
