﻿using Auction.Application.DTOs.Users;
using Auction.Domain.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Application.Interfaces.Account
{
    public interface IAccountService
    {
        public Task<UserDTO> Register(RegisterDTO userDTO);

        public Task<UserDTO> GetCurrentUser(string email);
    }
}
