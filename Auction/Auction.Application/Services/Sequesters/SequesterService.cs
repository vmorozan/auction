﻿using Auction.Application.DTOs.Common;
using Auction.Application.DTOs.Sequesters;
using Auction.Application.Helpers.Models;
using Auction.Application.Interfaces.Sequesters;
using Auction.Application.Paginations.Interfaces.Sequesters;
using Auction.Domain;
using Auction.Domain.Filters;
using Auction.Domain.Others;
using Auction.Infrastructure.Interfaces;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auction.Application.Services.Sequesters
{
    public class SequesterService : ISequesterService
    {
        private readonly ISequesterRepository _sequesterRepository;
        private readonly IMapper _mapper;
        private readonly ISequesterPaginator _paginator;

        public SequesterService(ISequesterRepository sequesterRepository, IMapper mapper, ISequesterPaginator paginator)
        {
            _sequesterRepository = sequesterRepository;
            _mapper = mapper;
            _paginator = paginator;
        }

        public async Task<Result<ResultListDTO<SequesterDTO>>> Gets(string filter)
        {
            var sequesters = _sequesterRepository.Gets();

            if (String.IsNullOrEmpty(filter))
            {
                var list = _mapper.Map<IReadOnlyList<Sequester>, IReadOnlyList<SequesterDTO>>(await sequesters.ToListAsync());

                var resWObj = new ResultListDTO<SequesterDTO>()
                {
                    Objects = list as List<SequesterDTO>,
                    ItemPerPage = 5,
                    CurrentPage = 1,
                    TotalItems = string.IsNullOrEmpty("") ? await sequesters.CountAsync() : list.Count
                };

                return new Result<ResultListDTO<SequesterDTO>>()
                {
                    ResultObject = resWObj,
                    ResultCode = ResultCode.SUCCESS
                };
            }

            var filterObj = JsonConvert.DeserializeObject<SequesterFilters>(filter);

            var sequesterMapped = _mapper.Map<IReadOnlyList<Sequester>, IReadOnlyList<SequesterDTO>>(
                await _paginator.SortedSequesters(filterObj, sequesters)
            );

            var resObj = new ResultListDTO<SequesterDTO>()
            {
                Objects = sequesterMapped as List<SequesterDTO>,
                ItemPerPage = sequesterMapped.Count,
                CurrentPage = filterObj.CurrentPage,
                TotalItems = string.IsNullOrEmpty(filterObj.SearchParam) ? await sequesters.CountAsync() : sequesterMapped.Count
            };

            return new Result<ResultListDTO<SequesterDTO>>()
            {
                ResultObject = resObj,
                ResultCode = ResultCode.SUCCESS
            };
        }

        public async Task<Result<SequesterDTO>> Create(SequesterDTO sequester)
        {
            var sequesterKey = Guid.NewGuid();
            var seq = _mapper.Map<SequesterDTO, Sequester>(sequester);
            var filterList = new List<CommonFiltersSequester>();

            sequester.Filters.ToList().ForEach(x => {
                filterList.Add(new CommonFiltersSequester()
                {
                    SequesterKey = sequesterKey,
                    FilterKey = x.Key
                });
            });


            seq.CreatorKey = Guid.Parse("A13F7BCC-5FD2-4D77-8419-966171649612");
            seq.DateRegistration = DateTime.Now;
            seq.Status = SequesterStatus.InProgress;
            seq.Type = AuctionType.FirstAuction;
            seq.Key = sequesterKey;
            seq.Filters = filterList;

            var result = await _sequesterRepository.Create(seq);

            return new Result<SequesterDTO>()
            {
                ResultObject = sequester,
                ResultCode = ResultCode.SUCCESS
            };
        }

        public async Task<Result<SequesterDTO>> AddFilters(SequesterDTO sequester)
        {
            var seq = _mapper.Map<SequesterDTO, Sequester>(sequester);

            var result = await _sequesterRepository.Create(seq);

            return new Result<SequesterDTO>()
            {
                ResultObject = _mapper.Map<Sequester, SequesterDTO>(result),
                ResultCode = ResultCode.SUCCESS
            };
        }

        public async Task<Result<SequesterDTO>> Get(Guid sequesterKey)
        {
            var result = await _sequesterRepository.Get(sequesterKey);

            return new Result<SequesterDTO>()
            {
                ResultObject = _mapper.Map<Sequester, SequesterDTO>(result),
                ResultCode = ResultCode.SUCCESS
            };
        }
    }
}
