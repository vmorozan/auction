﻿using Auction.Application.DTOs.Users;
using Auction.Application.Helpers.Models;
using Auction.Application.Interfaces.Users;
using Auction.Application.Paginations.Interfaces.Users;
using Auction.Domain.Users;
using Auction.Infrastructure.Interfaces;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Auction.Application.Services.Users
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        private readonly IUserPaginator _paginator;

        public UserService(IUserRepository userRepository, IMapper mapper, IUserPaginator paginator)
        {
            _userRepository = userRepository;
            _mapper = mapper;
            _paginator = paginator;
        }

        public async Task<bool> Delete(Guid key)
        {
            return await _userRepository.Delete(key);
        }

        public async Task<ResultListDTO<UserListDTO>> Gets(string filter)
        {
            var users = _userRepository.Gets();

            if (String.IsNullOrEmpty(filter))
            {
                var list = _mapper.Map<IReadOnlyList<User>, IReadOnlyList<UserListDTO>>(await users.ToListAsync());

                return new ResultListDTO<UserListDTO>()
                {
                    Objects = list as List<UserListDTO>,
                    ItemPerPage = 5,
                    CurrentPage = 1,
                    TotalItems = string.IsNullOrEmpty("") ? await users.CountAsync() : list.Count
                };
            }

            var filterObj = JsonConvert.DeserializeObject<UserFilter>(filter);

            var usersMapped = _mapper.Map<IReadOnlyList<User>, IReadOnlyList<UserListDTO>>(
                await _paginator.SortedUsers(filterObj, users)
            );

            return new ResultListDTO<UserListDTO>()
            {
                Objects = usersMapped as List<UserListDTO>,
                ItemPerPage = usersMapped.Count,
                CurrentPage = filterObj.CurrentPage,
                TotalItems = string.IsNullOrEmpty(filterObj.SearchParam) ? await users.CountAsync() : usersMapped.Count
            };
        } 
    }
}
