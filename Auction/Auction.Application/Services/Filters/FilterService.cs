﻿using Auction.Application.DTOs.Common;
using Auction.Application.DTOs.Executor;
using Auction.Application.DTOs.Filters.Categories;
using Auction.Application.DTOs.Filters.Cities;
using Auction.Application.DTOs.Filters.Commons;
using Auction.Application.Helpers.Models;
using Auction.Application.Interfaces.Filters;
using Auction.Application.Paginations.Interfaces.Filters;
using Auction.Domain.Addresses;
using Auction.Domain.Filters;
using Auction.Infrastructure.Interfaces;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Auction.Application.Services.Filters
{
    public class FilterService : IFilterService
    {
        private readonly IFilterRepository _filterRepository;
        private readonly ICommonFilterPaginator _paginator;
        private readonly ICategoryPaginator _categoryPaginator;
        private readonly ISubCategoryPaginator _subCategoryPaginator;
        private readonly IMapper _mapper;

        public FilterService(
            IFilterRepository filterRepository,
            IMapper mapper,
            ICommonFilterPaginator paginator,
            ICategoryPaginator categoryPaginator,
            ISubCategoryPaginator subCategoryPaginator)
        {
            _filterRepository = filterRepository;
            _mapper = mapper;
            _paginator = paginator;
            _categoryPaginator = categoryPaginator;
            _subCategoryPaginator = subCategoryPaginator;
        }

        public async Task<Result<IReadOnlyList<CategoryDTO>>> GetCategories()
        {
            var categories = await _filterRepository.GetCategories();

            return new Result<IReadOnlyList<CategoryDTO>>() {
                ResultCode = ResultCode.SUCCESS,
                ResultObject = _mapper.Map<IReadOnlyList<Category>, IReadOnlyList<CategoryDTO>>(categories)
            };
        }

        public async Task<Result<ResultListDTO<CategoryDTO>>> GetListCategories(string filter)
        {
            var categories = _filterRepository.GetListCategories();

            var listDTO = new ResultListDTO<CategoryDTO>();
            if (String.IsNullOrEmpty(filter))
            {
                var list = _mapper.Map<IReadOnlyList<Category>, IReadOnlyList<CategoryDTO>>(await categories.ToListAsync());

                var initFilterObj = _mapper.Map<IReadOnlyList<Category>, IReadOnlyList<CategoryDTO>>(
                    await _categoryPaginator.SortedCategories(new CategoryFilter()
                    {
                        SearchParam = "",
                        SortingDirection = "",
                        SortingParam = "",
                        CurrentPage = 0,
                        ItemsPerPage = 5
                    }, categories)
                );

                listDTO = new ResultListDTO<CategoryDTO>()
                {
                    Objects = initFilterObj as List<CategoryDTO>,
                    ItemPerPage = 5,
                    CurrentPage = 1,
                    TotalItems = string.IsNullOrEmpty("") ? await categories.CountAsync() : list.Count
                };

                return new Result<ResultListDTO<CategoryDTO>>()
                {
                    ResultCode = ResultCode.SUCCESS,
                    ResultObject = listDTO
                };
            }

            var filterObj = JsonConvert.DeserializeObject<CategoryFilter>(filter);

            var filterMapped = _mapper.Map<IReadOnlyList<Category>, IReadOnlyList<CategoryDTO>>(
                await _categoryPaginator.SortedCategories(filterObj, categories)
            );

            listDTO = new ResultListDTO<CategoryDTO>()
            {
                Objects = filterMapped as List<CategoryDTO>,
                ItemPerPage = filterMapped.Count,
                CurrentPage = filterObj.CurrentPage,
                TotalItems = string.IsNullOrEmpty(filterObj.SearchParam) ? await categories.CountAsync() : filterMapped.Count
            };

            return new Result<ResultListDTO<CategoryDTO>>()
            {
                ResultCode = ResultCode.SUCCESS,
                ResultObject = listDTO
            };
        }

        public async Task<Result<ResultListDTO<SubCategoryDTO>>> GetListSubCategories(string filter)
        {
            var subCategories = _filterRepository.GetListSubCategories();

            var listDTO = new ResultListDTO<SubCategoryDTO>();
            if (String.IsNullOrEmpty(filter))
            {
                var list = _mapper.Map<IReadOnlyList<SubCategory>, IReadOnlyList<SubCategoryDTO>>(await subCategories.ToListAsync());

                var initFilterObj = _mapper.Map<IReadOnlyList<SubCategory>, IReadOnlyList<SubCategoryDTO>>(
                    await _subCategoryPaginator.SortedSubCategories(new SubCategoryFilter()
                    {
                        SearchParam = "",
                        SortingDirection = "",
                        SortingParam = "",
                        CurrentPage = 0,
                        ItemsPerPage = 5
                    }, subCategories)
                );

                listDTO = new ResultListDTO<SubCategoryDTO>()
                {
                    Objects = initFilterObj as List<SubCategoryDTO>,
                    ItemPerPage = 5,
                    CurrentPage = 1,
                    TotalItems = string.IsNullOrEmpty("") ? await subCategories.CountAsync() : list.Count
                };

                return new Result<ResultListDTO<SubCategoryDTO>>()
                {
                    ResultCode = ResultCode.SUCCESS,
                    ResultObject = listDTO
                };
            }

            var filterObj = JsonConvert.DeserializeObject<SubCategoryFilter>(filter);

            var filterMapped = _mapper.Map<IReadOnlyList<SubCategory>, IReadOnlyList<SubCategoryDTO>>(
                await _subCategoryPaginator.SortedSubCategories(filterObj, subCategories)
            );

            listDTO = new ResultListDTO<SubCategoryDTO>()
            {
                Objects = filterMapped as List<SubCategoryDTO>,
                ItemPerPage = filterMapped.Count,
                CurrentPage = filterObj.CurrentPage,
                TotalItems = string.IsNullOrEmpty(filterObj.SearchParam) ? await subCategories.CountAsync() : filterMapped.Count
            };

            return new Result<ResultListDTO<SubCategoryDTO>>()
            {
                ResultCode = ResultCode.SUCCESS,
                ResultObject = listDTO
            };
        }

        public async Task<CategoryDTO> GetCategoryByName(string name)
        {
            var categories = await _filterRepository.GetCategoryByName(name);

            return _mapper.Map<Category, CategoryDTO>(categories);
        }

        public async Task<Result<IReadOnlyList<SubCategoryDTO>>> GetSubCategories(Guid categoryKey)
        {
            var subCategories = await _filterRepository.GetSubCategories(categoryKey);

            return new Result<IReadOnlyList<SubCategoryDTO>>()
            {
                ResultCode = ResultCode.SUCCESS,
                ResultObject = _mapper.Map<IReadOnlyList<SubCategory>, IReadOnlyList<SubCategoryDTO>>(subCategories)
            };
        }

        public async Task<CategoryDTO> AddCategory(CategoryDTO categoryDTO)
        {
            var categoryEntity = _mapper.Map<CategoryDTO, Category>(categoryDTO);

            var categories = await _filterRepository.AddCategory(categoryEntity);

            return _mapper.Map<Category, CategoryDTO>(categories);
        }

        public async Task<SubCategoryDTO> AddSubCategory(SubCategoryDTO subCategoryDTO)
        {
            return _mapper.Map<SubCategory, SubCategoryDTO>(
                await _filterRepository.AddSubCategory(
                    _mapper.Map<SubCategoryDTO, SubCategory>(subCategoryDTO)
                    )
                );
        }

        public async Task<CommonFilterDTO> AddCommonFilter(CommonFilterDTO commonFilterDTO)
        {
            return _mapper.Map<CommonFilters, CommonFilterDTO>(
                await _filterRepository.AddCommonFilter(
                        _mapper.Map<CommonFilterDTO, CommonFilters>(commonFilterDTO)
                    )
                );
        }

        public async Task<IReadOnlyList<CityDTO>> GetCities()
        {
            return _mapper.Map<IReadOnlyList<City>, IReadOnlyList<CityDTO>>(await _filterRepository.GetCities());
        }

        public async Task<Result<ResultListDTO<CommonFilterDTO>>> GetsCommonFilter(string filter)
        {
            var filters = _filterRepository.GetsCommonFilter();
            var listDTO = new ResultListDTO<CommonFilterDTO>();
            if (String.IsNullOrEmpty(filter))
            {
                var list = _mapper.Map<IReadOnlyList<CommonFilters>, IReadOnlyList<CommonFilterDTO>>(await filters.ToListAsync());

                var initFilterObj = _mapper.Map<IReadOnlyList<CommonFilters>, IReadOnlyList<CommonFilterDTO>>(
                    await _paginator.SortedCommonFilters(new EntityFilter() {
                        SearchParam = "",
                        SortingDirection = "",
                        SortingParam = "",
                        CurrentPage = 0,
                        ItemsPerPage = 5
                    }, filters)
                );

                listDTO = new ResultListDTO<CommonFilterDTO>()
                {
                    Objects = initFilterObj as List<CommonFilterDTO>,
                    ItemPerPage = 5,
                    CurrentPage = 1,
                    TotalItems = string.IsNullOrEmpty("") ? await filters.CountAsync() : list.Count
                };

                return new Result<ResultListDTO<CommonFilterDTO>>()
                {
                    ResultCode = ResultCode.SUCCESS,
                    ResultObject = listDTO
                };
            }

            var filterObj = JsonConvert.DeserializeObject<EntityFilter>(filter);

            var filterMapped = _mapper.Map<IReadOnlyList<CommonFilters>, IReadOnlyList<CommonFilterDTO>>(
                await _paginator.SortedCommonFilters(filterObj, filters)
            );

            listDTO = new ResultListDTO<CommonFilterDTO>()
            {
                Objects = filterMapped as List<CommonFilterDTO>,
                ItemPerPage = filterMapped.Count,
                CurrentPage = filterObj.CurrentPage,
                TotalItems = string.IsNullOrEmpty(filterObj.SearchParam) ? await filters.CountAsync() : filterMapped.Count
            };

            return new Result<ResultListDTO<CommonFilterDTO>>()
            {
                ResultCode = ResultCode.SUCCESS,
                ResultObject = listDTO
            };
        }

        public async Task<IReadOnlyList<CommonFilterDTO>> GetListCommonFilter(CommonFilterDTO filterDto)
        {
            var filter = _mapper.Map<CommonFilterDTO, CommonFilters>(filterDto);
            var result = _filterRepository.GetListCommonFilter(filter);
            return _mapper.Map<IReadOnlyList<CommonFilters>, IReadOnlyList<CommonFilterDTO>>(await result.ToListAsync());
        }

        public async Task<IReadOnlyList<CommonFilterDTO>> GetListCommonFilterByCategory(Guid categoryKey)
        {
            var result = _filterRepository.GetListCommonFilterByCategory(categoryKey);
            return _mapper.Map<IReadOnlyList<CommonFilters>, IReadOnlyList<CommonFilterDTO>>(await result.ToListAsync());
        }

        public async Task<IReadOnlyList<CommonFilterDTO>> GetListCommonFilterBySubCategory(Guid categoryKey, Guid subCategoryKey)
        {
            var result = _filterRepository.GetListCommonFilterBySubCategory(categoryKey, subCategoryKey);
            return _mapper.Map<IReadOnlyList<CommonFilters>, IReadOnlyList<CommonFilterDTO>>(await result.ToListAsync());
        }
    }
}
