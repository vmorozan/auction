﻿using Auction.Application.DTOs.Common;
using Auction.Application.DTOs.Executor;
using Auction.Application.Helpers.Models;
using Auction.Application.Interfaces.Executors;
using Auction.Application.Paginations.Interfaces.Executors;
using Auction.Domain.Addresses;
using Auction.Domain.Executors;
using Auction.Domain.Others;
using Auction.Domain.Persons;
using Auction.Domain.Users;
using Auction.Infrastructure.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Auction.Application.Services.Executors
{
    public class ExecutorService : IExecutorService
    {
        private readonly IExecutorRepository _executorRepository;
        private readonly IMapper _mapper;
        private readonly IExecutorPaginator _paginator;
        private readonly UserManager<User> _userManager;

        public ExecutorService(
            IExecutorRepository executorRepository,
            IMapper mapper,
            IExecutorPaginator paginator,
            UserManager<User> userManager)
        {
            _executorRepository = executorRepository;
            _mapper = mapper;
            _paginator = paginator;
            _userManager = userManager;
        }


        public async Task<Result<bool>> Add(ExecutorDTO executor)
        {
            var user = new User()
            {
                UserName = string.IsNullOrEmpty(executor.UserName) ? executor.Email.Split('@')[0] : executor.Email,
                Email = executor.Email,
                UserType = UserType.Executor,
                Password = Guid.NewGuid().ToString(),
            };

            if (!string.IsNullOrEmpty(executor.FirstName) || !string.IsNullOrEmpty(executor.LastName))
            {
                user.PersonKey = Guid.NewGuid();
                user.Person = new Person()
                {
                    FirstName = executor.FirstName,
                    LastName = executor.LastName
                };

                if (!string.IsNullOrEmpty(executor.Street) || !string.IsNullOrEmpty(executor.City) || !string.IsNullOrEmpty(executor.ZipCode))
                {
                    user.Person.AddressKey = Guid.NewGuid();
                    user.Person.Address = new Address()
                    {
                        City = executor.City,
                        Street = executor.Street,
                        ZipCode = executor.ZipCode,
                        Country = "Moldova"
                    };
                }
            }

            if (!string.IsNullOrEmpty(executor.ExecutorNumber))
            {
                user.ExecutorDetailsKey = Guid.NewGuid();
                user.ExecutorDetails = new Executor()
                {
                    ExecutorNumber = executor.ExecutorNumber
                };
            }

            var result = await _executorRepository.Add(user);

            return new Result<bool>() {
                ResultCode = ResultCode.SUCCESS,
                ResultObject = result
            };
        }

        public async Task<Result<bool>> Delete(Guid key)
        {
            var result = await _executorRepository.Delete(key);

            return new Result<bool>() {
                ResultCode = result == true ? ResultCode.SUCCESS : ResultCode.FAILED,
                ResultObject = result
            };
        }

        public async Task<Result<ResultListDTO<ExecutorListDTO>>> Gets(string filter)
        {
            var users = _executorRepository.Gets();
            var listDTO = new ResultListDTO<ExecutorListDTO>();

            if (String.IsNullOrEmpty(filter))
            {
                var list = _mapper.Map<IReadOnlyList<User>, IReadOnlyList<ExecutorListDTO>>(await users.ToListAsync());

                listDTO = new ResultListDTO<ExecutorListDTO>()
                {
                    Objects = list as List<ExecutorListDTO>,
                    ItemPerPage = 5,
                    CurrentPage = 1,
                    TotalItems = string.IsNullOrEmpty("") ? await users.CountAsync() : list.Count
                };

                return new Result<ResultListDTO<ExecutorListDTO>>()
                {
                    ResultCode = ResultCode.SUCCESS,
                    ResultObject = listDTO
                };
            }

            var filterObj = JsonConvert.DeserializeObject<ExecutorFilter>(filter);

            var usersMapped = _mapper.Map<IReadOnlyList<User>, IReadOnlyList<ExecutorListDTO>>(
                await _paginator.SortedExecutors(filterObj, users)
            );

            listDTO = new ResultListDTO<ExecutorListDTO>()
            {
                Objects = usersMapped as List<ExecutorListDTO>,
                ItemPerPage = usersMapped.Count,
                CurrentPage = filterObj.CurrentPage,
                TotalItems = string.IsNullOrEmpty(filterObj.SearchParam) ? await users.CountAsync() : usersMapped.Count
            };

            return new Result<ResultListDTO<ExecutorListDTO>>()
            {
                ResultCode = ResultCode.SUCCESS,
                ResultObject = listDTO
            };
        }

        public Task<Result<ExecutorDTO>> Update(ExecutorDTO executor)
        {
            throw new NotImplementedException();
        }
    }
}
