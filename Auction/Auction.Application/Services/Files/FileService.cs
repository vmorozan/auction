﻿using Auction.Application.DTOs.Files;
using Auction.Application.Interfaces.Files;
using Auction.Domain.Files;
using Auction.Infrastructure.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Auction.Application.Services.Files
{
    public class FileService : IFileService
    {
        private readonly IFileRepository _fileRepository;
        private readonly IMapper _mapper;

        public FileService(
            IFileRepository fileRepository,
            IMapper mapper)
        {
            _fileRepository = fileRepository;
            _mapper = mapper;
        }

        public async Task<FileDTO> Preview(Guid sequesterKey)
        {
            var res = await _fileRepository.Preview(sequesterKey).FirstOrDefaultAsync();
            return _mapper.Map<FileEntity, FileDTO>(res);
        }

        public async Task<FileDTO> Upload(UploadFileDTO uploadFile)
        {
            if (uploadFile.File?.Length > 0)
            {
                var fileName = Path.GetFileName(uploadFile.File.FileName);
                var fileExtension = Path.GetExtension(fileName);
                var fileEntity = new FileEntity()
                {
                    Name = fileName,
                    MimeType = uploadFile.File.ContentType,
                    OrderedId = uploadFile.OrderedId,
                    Created = DateTime.Now
                };

                using (var target = new MemoryStream())
                {
                    uploadFile.File.CopyTo(target);
                    fileEntity.FileType = target.ToArray();
                }

                return _mapper.Map<FileEntity, FileDTO>(await _fileRepository.Upload(fileEntity));
            }
            return _mapper.Map<FileEntity, FileDTO>(new FileEntity());
        }
    }
}
