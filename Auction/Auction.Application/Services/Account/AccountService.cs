﻿using Auction.Application.DTOs.Users;
using Auction.Application.Interfaces.Account;
using Auction.Application.Interfaces.Tokens;
using Auction.Domain.Addresses;
using Auction.Domain.Others;
using Auction.Domain.Persons;
using Auction.Domain.Users;
using Auction.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Threading.Tasks;

namespace Auction.Application.Services.Account
{
    public class AccountService : IAccountService
    {
        private readonly IAccountRepository _accountRepository;
        private readonly ITokenService _tokenService;
        private readonly UserManager<User> _userManager;

        public AccountService(
            IAccountRepository accountRepository,
            ITokenService tokenService,
            UserManager<User> userManager)
        {
            _accountRepository = accountRepository;
            _tokenService = tokenService;
            _userManager = userManager;
        }

        public async Task<UserDTO> GetCurrentUser(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);

            if (user == null) return null;

            return new UserDTO
            {
                Email = user.Email,
                Token = _tokenService.CreateToken(user),
                UserName = user.UserName
            };
        }

        public async Task<UserDTO> Register(RegisterDTO userDTO)
        {
            var user = new User() {
                UserName = string.IsNullOrEmpty(userDTO.UserName) ? userDTO.Email.Split('@')[0] : userDTO.Email,
                Email = userDTO.Email,
                UserType = UserType.SimpleUser,
                Password = Guid.NewGuid().ToString()
            };

            if (!string.IsNullOrEmpty(userDTO.FirstName) || !string.IsNullOrEmpty(userDTO.LastName))
            {
                user.UserType = UserType.Executor;
                user.PersonKey = Guid.NewGuid();
                user.Person = new Person()
                {
                    FirstName = userDTO.FirstName,
                    LastName = userDTO.LastName
                };

                if (!string.IsNullOrEmpty(userDTO.Street) || !string.IsNullOrEmpty(userDTO.City))
                {
                    user.Person.AddressKey = Guid.NewGuid();
                    user.Person.Address = new Address()
                    {
                        City = userDTO.City,
                        Street = userDTO.Street,
                        Country = "Moldova"
                    };
                }
            }
            var result = await _accountRepository.Register(user);
            var token = _tokenService.CreateToken(user);

            return new UserDTO()
            {
                UserName = result.UserName,
                Email = result.Email,
                Token = token,
                Street = userDTO.Street,
                City = userDTO.City,
                FirstName = userDTO.FirstName,
                LastName = userDTO.LastName
            };
        }
    }
}
