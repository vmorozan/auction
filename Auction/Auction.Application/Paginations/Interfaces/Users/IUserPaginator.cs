﻿using Auction.Domain.Users;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auction.Application.Paginations.Interfaces.Users
{
    public interface IUserPaginator
    {
        Task<IReadOnlyList<User>> SortedUsers(UserFilter filter, IQueryable<User> users);
    }
}
