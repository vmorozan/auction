﻿using Auction.Domain.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Application.Paginations.Interfaces.Filters
{
    public interface ISubCategoryPaginator
    {
        Task<IReadOnlyList<SubCategory>> SortedSubCategories(SubCategoryFilter filter, IQueryable<SubCategory> categories);
    }
}
