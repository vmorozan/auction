﻿using Auction.Domain.Filters;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auction.Application.Paginations.Interfaces.Filters
{
    public interface ICategoryPaginator
    {
        Task<IReadOnlyList<Category>> SortedCategories(CategoryFilter filter, IQueryable<Category> categories);
    }
}
