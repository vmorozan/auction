﻿using Auction.Domain;
using Auction.Domain.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Application.Paginations.Interfaces.Sequesters
{
    public interface ISequesterPaginator
    {
        Task<IReadOnlyList<Sequester>> SortedSequesters(SequesterFilters filter, IQueryable<Sequester> users);
    }
}
