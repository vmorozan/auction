﻿using Auction.Domain.Executors;
using Auction.Domain.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Application.Paginations.Interfaces.Executors
{
    public interface IExecutorPaginator
    {
        Task<IReadOnlyList<User>> SortedExecutors(ExecutorFilter filter, IQueryable<User> users);
    }
}
