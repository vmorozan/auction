﻿using Auction.Application.Paginations.Interfaces.Users;
using Auction.Domain.Users;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Application.Paginations.Users
{
    public class UserPaginator : IUserPaginator
    {
        public async Task<IReadOnlyList<User>> SortedUsers(UserFilter filter, IQueryable<User> users)
        {
            if (!string.IsNullOrEmpty(filter.SearchParam))
            {
                users = users.Where(x => x.UserName.Contains(filter.SearchParam) ||
                                    x.Person.FirstName.Contains(filter.SearchParam) ||
                                    x.Person.LastName.Contains(filter.SearchParam));
            }

            if (string.IsNullOrEmpty(filter.SortingParam))
            {
                if (string.IsNullOrEmpty(filter.SortingDirection) || filter.SortingParam.Equals("asc"))
                    users = users.OrderBy(x => x.UserName);
                else
                    users = users.OrderByDescending(x => x.UserName);
            }
            else
            {
                switch (filter.SortingParam)
                {
                    case "firstname":
                        {
                            if (filter.SortingParam.Equals("asc"))
                            {
                                users = users.OrderBy(x => x.Person.FirstName);
                                break;
                            }
                            users = users.OrderByDescending(x => x.Person.FirstName);
                            break;
                        }
                    case "lastname":
                        {
                            if (filter.SortingParam.Equals("asc"))
                            {
                                users = users.OrderBy(x => x.Person.LastName);
                                break;
                            }
                            users = users.OrderByDescending(x => x.Person.LastName);
                            break;
                        }
                    default:
                        users = users.OrderByDescending(x => x.UserName);
                        break;
                }
            }

            return await users
               .Skip(filter.CurrentPage * filter.ItemsPerPage)
               .Take(filter.ItemsPerPage)
               .ToListAsync();
        }

    }
}
