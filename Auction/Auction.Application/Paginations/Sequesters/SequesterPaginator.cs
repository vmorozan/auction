﻿using Auction.Application.Paginations.Interfaces.Sequesters;
using Auction.Domain;
using Auction.Domain.Filters;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auction.Application.Paginations.Sequesters
{
    public class SequesterPaginator : ISequesterPaginator
    {
        public async Task<IReadOnlyList<Sequester>> SortedSequesters(SequesterFilters filter, IQueryable<Sequester> sequesters)
        {
            if (!string.IsNullOrEmpty(filter.SearchParam))
            {
                sequesters = sequesters.Where(x => x.Name.Contains(filter.SearchParam));
            }

            if (string.IsNullOrEmpty(filter.SortingParam))
            {
                if (string.IsNullOrEmpty(filter.SortingDirection) || filter.SortingParam.Equals("asc"))
                    sequesters = sequesters.OrderBy(x => x.Name);
                else
                    sequesters = sequesters.OrderByDescending(x => x.Name);
            }
            else
            {
                switch (filter.SortingParam)
                {
                    case "name":
                        {
                            if (filter.SortingParam.Equals("asc"))
                            {
                                sequesters = sequesters.OrderBy(x => x.Name);
                                break;
                            }
                            sequesters = sequesters.OrderByDescending(x => x.Name);
                            break;
                        }
                    default:
                        sequesters = sequesters.OrderByDescending(x => x.Name);
                        break;
                }
            }

            return await sequesters
               .Skip(filter.CurrentPage * filter.ItemsPerPage)
               .Take(filter.ItemsPerPage)
               .ToListAsync();
        }
    }
}
