﻿using Auction.Application.Paginations.Interfaces.Filters;
using Auction.Domain.Filters;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auction.Application.Paginations.Filters
{
    public class SubCategoryPaginator : ISubCategoryPaginator
    {
        public async Task<IReadOnlyList<SubCategory>> SortedSubCategories(SubCategoryFilter filter, IQueryable<SubCategory> subCategories)
        {
            if (!string.IsNullOrEmpty(filter.SearchParam))
            {
                subCategories = subCategories.Where(x => x.Name.Contains(filter.SearchParam) ||
                                                   x.Category.Name.Contains(filter.SearchParam));
            }

            if (string.IsNullOrEmpty(filter.SortingParam))
            {
                if (string.IsNullOrEmpty(filter.SortingDirection) || filter.SortingParam.Equals("asc"))
                    subCategories = subCategories.OrderBy(x => x.Name);
                else
                    subCategories = subCategories.OrderByDescending(x => x.Name);
            }
            else
            {
                switch (filter.SortingParam)
                {
                    case "name":
                        {
                            if (filter.SortingParam.Equals("asc"))
                            {
                                subCategories = subCategories.OrderBy(x => x.Name);
                                break;
                            }
                            subCategories = subCategories.OrderByDescending(x => x.Name);
                            break;
                        }
                    case "categoryname":
                        {
                            if (filter.SortingParam.Equals("asc"))
                            {
                                subCategories = subCategories.OrderBy(x => x.Category.Name);
                                break;
                            }
                            subCategories = subCategories.OrderByDescending(x => x.Category.Name);
                            break;
                        }
                    default:
                        subCategories = subCategories.OrderByDescending(x => x.Name);
                        break;
                }
            }

            return await subCategories
               .Skip(filter.CurrentPage * filter.ItemsPerPage)
               .Take(filter.ItemsPerPage)
               .ToListAsync();
        }
    }
}
