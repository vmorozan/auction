﻿using Auction.Application.Paginations.Interfaces.Filters;
using Auction.Domain.Filters;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Application.Paginations.Filters
{
    public class CategoryPaginator : ICategoryPaginator
    {
        public async Task<IReadOnlyList<Category>> SortedCategories(CategoryFilter filter, IQueryable<Category> categories)
        {
            if (!string.IsNullOrEmpty(filter.SearchParam))
            {
                categories = categories.Where(x => x.Name.Contains(filter.SearchParam));
            }

            if (string.IsNullOrEmpty(filter.SortingParam))
            {
                if (string.IsNullOrEmpty(filter.SortingDirection) || filter.SortingParam.Equals("asc"))
                    categories = categories.OrderBy(x => x.Name);
                else
                    categories = categories.OrderByDescending(x => x.Name);
            }
            else
            {
                switch (filter.SortingParam)
                {
                    case "name":
                        {
                            if (filter.SortingParam.Equals("asc"))
                            {
                                categories = categories.OrderBy(x => x.Name);
                                break;
                            }
                            categories = categories.OrderByDescending(x => x.Name);
                            break;
                        }
                    default:
                        categories = categories.OrderByDescending(x => x.Name);
                        break;
                }
            }

            return await categories
               .Skip(filter.CurrentPage * filter.ItemsPerPage)
               .Take(filter.ItemsPerPage)
               .ToListAsync();
        }
    }
}
