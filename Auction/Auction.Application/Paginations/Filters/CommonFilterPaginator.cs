﻿using Auction.Application.Paginations.Interfaces.Filters;
using Auction.Domain.Filters;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Application.Paginations.Filters
{
    public class CommonFilterPaginator : ICommonFilterPaginator
    {
        public async Task<IReadOnlyList<CommonFilters>> SortedCommonFilters(EntityFilter filter, IQueryable<CommonFilters> commonFilters)
        {
            if (!string.IsNullOrEmpty(filter.SearchParam))
            {
                commonFilters = commonFilters.Where(x => x.Name.Contains(filter.SearchParam) ||
                                    x.Value.Contains(filter.SearchParam) ||
                                    x.Category.Name.Contains(filter.SearchParam) ||
                                    x.SubCategory.Name.Contains(filter.SearchParam));
            }

            if (string.IsNullOrEmpty(filter.SortingParam))
            {
                if (string.IsNullOrEmpty(filter.SortingDirection) || filter.SortingParam.Equals("asc"))
                    commonFilters = commonFilters.OrderBy(x => x.Name);
                else
                    commonFilters = commonFilters.OrderByDescending(x => x.Name);
            }
            else
            {
                switch (filter.SortingParam)
                {
                    case "name":
                        {
                            if (filter.SortingParam.Equals("asc"))
                            {
                                commonFilters = commonFilters.OrderBy(x => x.Name);
                                break;
                            }
                            commonFilters = commonFilters.OrderByDescending(x => x.Name);
                            break;
                        }
                    case "value":
                        {
                            if (filter.SortingParam.Equals("asc"))
                            {
                                commonFilters = commonFilters.OrderBy(x => x.Value);
                                break;
                            }
                            commonFilters = commonFilters.OrderByDescending(x => x.Value);
                            break;
                        }
                    case "categoryname":
                        {
                            if (filter.SortingParam.Equals("asc"))
                            {
                                commonFilters = commonFilters.OrderBy(x => x.Category.Name);
                                break;
                            }
                            commonFilters = commonFilters.OrderByDescending(x => x.Category.Name);
                            break;
                        }
                    case "subcategoryname":
                        {
                            if (filter.SortingParam.Equals("asc"))
                            {
                                commonFilters = commonFilters.OrderBy(x => x.SubCategory.Name);
                                break;
                            }
                            commonFilters = commonFilters.OrderByDescending(x => x.SubCategory.Name);
                            break;
                        }
                    default:
                        commonFilters = commonFilters.OrderByDescending(x => x.Name);
                        break;
                }
            }

            return await commonFilters
               .Skip(filter.CurrentPage * filter.ItemsPerPage)
               .Take(filter.ItemsPerPage)
               .ToListAsync();
        }
    }
}
