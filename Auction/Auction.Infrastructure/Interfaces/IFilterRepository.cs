﻿using Auction.Domain.Addresses;
using Auction.Domain.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auction.Infrastructure.Interfaces
{
    public interface IFilterRepository
    {
        Task<IReadOnlyList<Category>> GetCategories();

        IQueryable<Category> GetListCategories();

        IQueryable<SubCategory> GetListSubCategories();

        Task<Category> GetCategoryByName(string name);

        Task<IReadOnlyList<SubCategory>> GetSubCategories(Guid categoryKey);

        Task<IReadOnlyList<City>> GetCities();

        Task<Category> AddCategory(Category category);

        Task<SubCategory> AddSubCategory(SubCategory subCategory);

        Task<CommonFilters> AddCommonFilter(CommonFilters commonFilters);

        IQueryable<CommonFilters> GetsCommonFilter();

        IQueryable<CommonFilters> GetListCommonFilter(CommonFilters commonFilters);

        IQueryable<CommonFilters> GetListCommonFilterByCategory(Guid categoryKey);

        IQueryable<CommonFilters> GetListCommonFilterBySubCategory(Guid categoryKey, Guid subCategoryKey);
    }
}
