﻿using Auction.Domain.Persons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Infrastructure.Interfaces
{
    public interface IPersonRepository 
    {
        Task<Person> AddAsync(Person person);

        Task<bool> DeleteAsync(Person person);

        Task<Person> UpdateAsync(Person person);
    }
}
