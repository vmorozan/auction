﻿using Auction.Domain.Users;
using System.Threading.Tasks;

namespace Auction.Infrastructure.Interfaces
{
    public interface IAccountRepository
    {
        public Task<User> Register(User user);
    }
}
