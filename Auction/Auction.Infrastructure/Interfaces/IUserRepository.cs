﻿using Auction.Domain.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Infrastructure.Interfaces
{
    public interface IUserRepository
    {
        IQueryable<User> Gets();

        Task<bool> Delete(Guid key);
    }
}
