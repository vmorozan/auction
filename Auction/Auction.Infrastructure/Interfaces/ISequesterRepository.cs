﻿using Auction.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Infrastructure.Interfaces
{
    public interface ISequesterRepository
    {
        IQueryable<Sequester> Gets();

        Task<Sequester> Create(Sequester sequester);

        Task<Sequester> Get(Guid sequesterKey);

        Task<Sequester> AddFilters(Sequester sequester);
    }
}
