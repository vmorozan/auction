﻿using Auction.Domain.Files;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Infrastructure.Interfaces
{
    public interface IFileRepository
    {
        IQueryable<FileEntity> Preview(Guid SequesterKey);

        Task<FileEntity> Upload(FileEntity file);
    }
}
