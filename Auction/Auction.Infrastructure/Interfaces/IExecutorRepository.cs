﻿using Auction.Domain.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Infrastructure.Interfaces
{
    public interface IExecutorRepository
    {
        IQueryable<User> Gets();

        Task<bool> Delete(Guid key);

        Task<bool> Add(User executor);

        Task<User> Update(User executor);
    }
}
