﻿using Auction.Domain.Addresses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Infrastructure.Interfaces
{
    public interface IAddressRepository
    {
        Task<Address> AddAsync(Address person);

        Task<bool> DeleteAsync(Address person);

        Task<Address> UpdateAsync(Address person);
    }
}
