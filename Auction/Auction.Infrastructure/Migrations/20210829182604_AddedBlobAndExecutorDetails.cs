﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Auction.Infrastructure.Migrations
{
    public partial class AddedBlobAndExecutorDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "ExecutorDetailsKey",
                table: "Users",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ExecutorDetails",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ExecutorNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExecutorDetails", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "Blobs",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    KeyEntity = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    MimeType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ImageData = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ExecutorDetailKey = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Blobs", x => x.Key);
                    table.ForeignKey(
                        name: "FK_Blobs_ExecutorDetails_ExecutorDetailKey",
                        column: x => x.ExecutorDetailKey,
                        principalTable: "ExecutorDetails",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Users_ExecutorDetailsKey",
                table: "Users",
                column: "ExecutorDetailsKey");

            migrationBuilder.CreateIndex(
                name: "IX_Blobs_ExecutorDetailKey",
                table: "Blobs",
                column: "ExecutorDetailKey");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_ExecutorDetails_ExecutorDetailsKey",
                table: "Users",
                column: "ExecutorDetailsKey",
                principalTable: "ExecutorDetails",
                principalColumn: "Key",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_ExecutorDetails_ExecutorDetailsKey",
                table: "Users");

            migrationBuilder.DropTable(
                name: "Blobs");

            migrationBuilder.DropTable(
                name: "ExecutorDetails");

            migrationBuilder.DropIndex(
                name: "IX_Users_ExecutorDetailsKey",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "ExecutorDetailsKey",
                table: "Users");
        }
    }
}
