﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Auction.Infrastructure.Migrations
{
    public partial class CategoriesIsNotUnique : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Sequesters_CategoryKey",
                table: "Sequesters");

            migrationBuilder.DropIndex(
                name: "IX_Sequesters_SubCategoryKey",
                table: "Sequesters");

            migrationBuilder.CreateIndex(
                name: "IX_Sequesters_CategoryKey",
                table: "Sequesters",
                column: "CategoryKey");

            migrationBuilder.CreateIndex(
                name: "IX_Sequesters_SubCategoryKey",
                table: "Sequesters",
                column: "SubCategoryKey");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Sequesters_CategoryKey",
                table: "Sequesters");

            migrationBuilder.DropIndex(
                name: "IX_Sequesters_SubCategoryKey",
                table: "Sequesters");

            migrationBuilder.CreateIndex(
                name: "IX_Sequesters_CategoryKey",
                table: "Sequesters",
                column: "CategoryKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Sequesters_SubCategoryKey",
                table: "Sequesters",
                column: "SubCategoryKey",
                unique: true);
        }
    }
}
