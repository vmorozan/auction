﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Auction.Infrastructure.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Addresses",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    City = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Country = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ZipCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Street = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Location = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Addresses", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "Regions",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Regions", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NormalizedUserName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NormalizedEmail = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    PasswordHash = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SecurityStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "bit", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "bit", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "Persons",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AddressKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Persons", x => x.Key);
                    table.ForeignKey(
                        name: "FK_Persons_Addresses_AddressKey",
                        column: x => x.AddressKey,
                        principalTable: "Addresses",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubCategories",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CategoryKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubCategories", x => x.Key);
                    table.ForeignKey(
                        name: "FK_SubCategories_Categories_CategoryKey",
                        column: x => x.CategoryKey,
                        principalTable: "Categories",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserKey = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Key);
                    table.ForeignKey(
                        name: "FK_Roles_Users_UserKey",
                        column: x => x.UserKey,
                        principalTable: "Users",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Filters",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CategoryKey = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    SubCategoryKey = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Filters", x => x.Key);
                    table.ForeignKey(
                        name: "FK_Filters_Categories_CategoryKey",
                        column: x => x.CategoryKey,
                        principalTable: "Categories",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Filters_SubCategories_SubCategoryKey",
                        column: x => x.SubCategoryKey,
                        principalTable: "SubCategories",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Claims",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserKey = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Claims", x => x.Key);
                    table.ForeignKey(
                        name: "FK_Claims_Roles_Key",
                        column: x => x.Key,
                        principalTable: "Roles",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Claims_Users_UserKey",
                        column: x => x.UserKey,
                        principalTable: "Users",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AdvertismentCars",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatorKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CategoryKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubCategoryKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Type = table.Column<int>(type: "int", nullable: false),
                    ModelKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    MarkKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    MatriculateKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    StateKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Comments = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RegionKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    WheelKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Price = table.Column<double>(type: "float", nullable: false),
                    DateRegistration = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateExpired = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvertismentCars", x => x.Key);
                    table.ForeignKey(
                        name: "FK_AdvertismentCars_Categories_CategoryKey",
                        column: x => x.CategoryKey,
                        principalTable: "Categories",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentCars_Countries_MatriculateKey",
                        column: x => x.MatriculateKey,
                        principalTable: "Countries",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentCars_Filters_MarkKey",
                        column: x => x.MarkKey,
                        principalTable: "Filters",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentCars_Filters_ModelKey",
                        column: x => x.ModelKey,
                        principalTable: "Filters",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentCars_Filters_StateKey",
                        column: x => x.StateKey,
                        principalTable: "Filters",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentCars_Filters_WheelKey",
                        column: x => x.WheelKey,
                        principalTable: "Filters",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentCars_Regions_RegionKey",
                        column: x => x.RegionKey,
                        principalTable: "Regions",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentCars_SubCategories_SubCategoryKey",
                        column: x => x.SubCategoryKey,
                        principalTable: "SubCategories",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentCars_Users_CreatorKey",
                        column: x => x.CreatorKey,
                        principalTable: "Users",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AdvertismentRealEstates",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatorKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CategoryKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubCategoryKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Type = table.Column<int>(type: "int", nullable: false),
                    AuthorAnnouncementKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    HousingFondKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TypeBuildingKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    StateKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Comments = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RegionKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Price = table.Column<double>(type: "float", nullable: false),
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvertismentRealEstates", x => x.Key);
                    table.ForeignKey(
                        name: "FK_AdvertismentRealEstates_Categories_CategoryKey",
                        column: x => x.CategoryKey,
                        principalTable: "Categories",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentRealEstates_Filters_AuthorAnnouncementKey",
                        column: x => x.AuthorAnnouncementKey,
                        principalTable: "Filters",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentRealEstates_Filters_HousingFondKey",
                        column: x => x.HousingFondKey,
                        principalTable: "Filters",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentRealEstates_Filters_StateKey",
                        column: x => x.StateKey,
                        principalTable: "Filters",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentRealEstates_Filters_TypeBuildingKey",
                        column: x => x.TypeBuildingKey,
                        principalTable: "Filters",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentRealEstates_Regions_RegionKey",
                        column: x => x.RegionKey,
                        principalTable: "Regions",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentRealEstates_SubCategories_SubCategoryKey",
                        column: x => x.SubCategoryKey,
                        principalTable: "SubCategories",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentRealEstates_Users_CreatorKey",
                        column: x => x.CreatorKey,
                        principalTable: "Users",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentCars_CategoryKey",
                table: "AdvertismentCars",
                column: "CategoryKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentCars_CreatorKey",
                table: "AdvertismentCars",
                column: "CreatorKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentCars_MarkKey",
                table: "AdvertismentCars",
                column: "MarkKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentCars_MatriculateKey",
                table: "AdvertismentCars",
                column: "MatriculateKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentCars_ModelKey",
                table: "AdvertismentCars",
                column: "ModelKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentCars_RegionKey",
                table: "AdvertismentCars",
                column: "RegionKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentCars_StateKey",
                table: "AdvertismentCars",
                column: "StateKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentCars_SubCategoryKey",
                table: "AdvertismentCars",
                column: "SubCategoryKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentCars_WheelKey",
                table: "AdvertismentCars",
                column: "WheelKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentRealEstates_AuthorAnnouncementKey",
                table: "AdvertismentRealEstates",
                column: "AuthorAnnouncementKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentRealEstates_CategoryKey",
                table: "AdvertismentRealEstates",
                column: "CategoryKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentRealEstates_CreatorKey",
                table: "AdvertismentRealEstates",
                column: "CreatorKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentRealEstates_HousingFondKey",
                table: "AdvertismentRealEstates",
                column: "HousingFondKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentRealEstates_RegionKey",
                table: "AdvertismentRealEstates",
                column: "RegionKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentRealEstates_StateKey",
                table: "AdvertismentRealEstates",
                column: "StateKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentRealEstates_SubCategoryKey",
                table: "AdvertismentRealEstates",
                column: "SubCategoryKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentRealEstates_TypeBuildingKey",
                table: "AdvertismentRealEstates",
                column: "TypeBuildingKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Claims_UserKey",
                table: "Claims",
                column: "UserKey");

            migrationBuilder.CreateIndex(
                name: "IX_Filters_CategoryKey",
                table: "Filters",
                column: "CategoryKey");

            migrationBuilder.CreateIndex(
                name: "IX_Filters_SubCategoryKey",
                table: "Filters",
                column: "SubCategoryKey");

            migrationBuilder.CreateIndex(
                name: "IX_Persons_AddressKey",
                table: "Persons",
                column: "AddressKey");

            migrationBuilder.CreateIndex(
                name: "IX_Roles_UserKey",
                table: "Roles",
                column: "UserKey");

            migrationBuilder.CreateIndex(
                name: "IX_SubCategories_CategoryKey",
                table: "SubCategories",
                column: "CategoryKey");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdvertismentCars");

            migrationBuilder.DropTable(
                name: "AdvertismentRealEstates");

            migrationBuilder.DropTable(
                name: "Claims");

            migrationBuilder.DropTable(
                name: "Persons");

            migrationBuilder.DropTable(
                name: "Countries");

            migrationBuilder.DropTable(
                name: "Filters");

            migrationBuilder.DropTable(
                name: "Regions");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Addresses");

            migrationBuilder.DropTable(
                name: "SubCategories");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Categories");
        }
    }
}
