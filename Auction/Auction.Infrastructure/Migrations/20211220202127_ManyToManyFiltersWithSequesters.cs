﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Auction.Infrastructure.Migrations
{
    public partial class ManyToManyFiltersWithSequesters : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CommonFiltersSequester");

            migrationBuilder.CreateTable(
                name: "SequesterFilters",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SequesterKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    FilterKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SequesterFilters", x => x.Key);
                    table.ForeignKey(
                        name: "FK_SequesterFilters_Filters_FilterKey",
                        column: x => x.FilterKey,
                        principalTable: "Filters",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SequesterFilters_Sequesters_SequesterKey",
                        column: x => x.SequesterKey,
                        principalTable: "Sequesters",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SequesterFilters_FilterKey",
                table: "SequesterFilters",
                column: "FilterKey");

            migrationBuilder.CreateIndex(
                name: "IX_SequesterFilters_SequesterKey",
                table: "SequesterFilters",
                column: "SequesterKey");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SequesterFilters");

            migrationBuilder.CreateTable(
                name: "CommonFiltersSequester",
                columns: table => new
                {
                    FiltersKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SequesterKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommonFiltersSequester", x => new { x.FiltersKey, x.SequesterKey });
                    table.ForeignKey(
                        name: "FK_CommonFiltersSequester_Filters_FiltersKey",
                        column: x => x.FiltersKey,
                        principalTable: "Filters",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CommonFiltersSequester_Sequesters_SequesterKey",
                        column: x => x.SequesterKey,
                        principalTable: "Sequesters",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CommonFiltersSequester_SequesterKey",
                table: "CommonFiltersSequester",
                column: "SequesterKey");
        }
    }
}
