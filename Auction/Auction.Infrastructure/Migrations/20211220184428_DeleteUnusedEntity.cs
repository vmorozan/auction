﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Auction.Infrastructure.Migrations
{
    public partial class DeleteUnusedEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdvertismentRealEstates");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AdvertismentRealEstates",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AuthorAnnouncementKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CategoryKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatorKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    HousingFondKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SequesterKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubCategoryKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TypeBuildingKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvertismentRealEstates", x => x.Key);
                    table.ForeignKey(
                        name: "FK_AdvertismentRealEstates_Categories_CategoryKey",
                        column: x => x.CategoryKey,
                        principalTable: "Categories",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentRealEstates_Filters_AuthorAnnouncementKey",
                        column: x => x.AuthorAnnouncementKey,
                        principalTable: "Filters",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentRealEstates_Filters_HousingFondKey",
                        column: x => x.HousingFondKey,
                        principalTable: "Filters",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentRealEstates_Filters_TypeBuildingKey",
                        column: x => x.TypeBuildingKey,
                        principalTable: "Filters",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentRealEstates_Sequesters_SequesterKey",
                        column: x => x.SequesterKey,
                        principalTable: "Sequesters",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentRealEstates_SubCategories_SubCategoryKey",
                        column: x => x.SubCategoryKey,
                        principalTable: "SubCategories",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentRealEstates_Users_CreatorKey",
                        column: x => x.CreatorKey,
                        principalTable: "Users",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentRealEstates_AuthorAnnouncementKey",
                table: "AdvertismentRealEstates",
                column: "AuthorAnnouncementKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentRealEstates_CategoryKey",
                table: "AdvertismentRealEstates",
                column: "CategoryKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentRealEstates_CreatorKey",
                table: "AdvertismentRealEstates",
                column: "CreatorKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentRealEstates_HousingFondKey",
                table: "AdvertismentRealEstates",
                column: "HousingFondKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentRealEstates_SequesterKey",
                table: "AdvertismentRealEstates",
                column: "SequesterKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentRealEstates_SubCategoryKey",
                table: "AdvertismentRealEstates",
                column: "SubCategoryKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentRealEstates_TypeBuildingKey",
                table: "AdvertismentRealEstates",
                column: "TypeBuildingKey",
                unique: true);
        }
    }
}
