﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Auction.Infrastructure.Migrations
{
    public partial class AddManyToManyFilterWithSequester : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Filters_Sequesters_SequesterKey",
                table: "Filters");

            migrationBuilder.DropForeignKey(
                name: "FK_Filters_Sequesters_SequesterKey1",
                table: "Filters");

            migrationBuilder.DropIndex(
                name: "IX_Filters_SequesterKey",
                table: "Filters");

            migrationBuilder.DropIndex(
                name: "IX_Filters_SequesterKey1",
                table: "Filters");

            migrationBuilder.DropColumn(
                name: "SequesterKey",
                table: "Filters");

            migrationBuilder.DropColumn(
                name: "SequesterKey1",
                table: "Filters");

            migrationBuilder.AlterColumn<decimal>(
                name: "BuyedPrice",
                table: "Sequesters",
                type: "decimal(12,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(12,2)");

            migrationBuilder.CreateTable(
                name: "CommonFiltersSequester",
                columns: table => new
                {
                    FiltersKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SequesterKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommonFiltersSequester", x => new { x.FiltersKey, x.SequesterKey });
                    table.ForeignKey(
                        name: "FK_CommonFiltersSequester_Filters_FiltersKey",
                        column: x => x.FiltersKey,
                        principalTable: "Filters",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CommonFiltersSequester_Sequesters_SequesterKey",
                        column: x => x.SequesterKey,
                        principalTable: "Sequesters",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CommonFiltersSequester_SequesterKey",
                table: "CommonFiltersSequester",
                column: "SequesterKey");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CommonFiltersSequester");

            migrationBuilder.AlterColumn<decimal>(
                name: "BuyedPrice",
                table: "Sequesters",
                type: "decimal(12,2)",
                nullable: false,
                defaultValue: 0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(12,2)",
                oldNullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "SequesterKey",
                table: "Filters",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "SequesterKey1",
                table: "Filters",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Filters_SequesterKey",
                table: "Filters",
                column: "SequesterKey");

            migrationBuilder.CreateIndex(
                name: "IX_Filters_SequesterKey1",
                table: "Filters",
                column: "SequesterKey1");

            migrationBuilder.AddForeignKey(
                name: "FK_Filters_Sequesters_SequesterKey",
                table: "Filters",
                column: "SequesterKey",
                principalTable: "Sequesters",
                principalColumn: "Key",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Filters_Sequesters_SequesterKey1",
                table: "Filters",
                column: "SequesterKey1",
                principalTable: "Sequesters",
                principalColumn: "Key",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
