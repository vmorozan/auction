﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Auction.Infrastructure.Migrations
{
    public partial class AddedUserTypeAndPersonForUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Claims_Users_UserKey",
                table: "Claims");

            migrationBuilder.DropForeignKey(
                name: "FK_Persons_Addresses_AddressKey",
                table: "Persons");

            migrationBuilder.AddColumn<DateTime>(
                name: "DateRegistration",
                table: "Users",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Users",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<Guid>(
                name: "PersonKey",
                table: "Users",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserType",
                table: "Users",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<Guid>(
                name: "AddressKey",
                table: "Persons",
                type: "uniqueidentifier",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.AlterColumn<Guid>(
                name: "UserKey",
                table: "Claims",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_PersonKey",
                table: "Users",
                column: "PersonKey",
                unique: true,
                filter: "[PersonKey] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Claims_Users_Key",
                table: "Claims",
                column: "Key",
                principalTable: "Users",
                principalColumn: "Key",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Claims_Users_UserKey",
                table: "Claims",
                column: "UserKey",
                principalTable: "Users",
                principalColumn: "Key",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Persons_Addresses_AddressKey",
                table: "Persons",
                column: "AddressKey",
                principalTable: "Addresses",
                principalColumn: "Key",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Roles_Users_Key",
                table: "Roles",
                column: "Key",
                principalTable: "Users",
                principalColumn: "Key",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Persons_PersonKey",
                table: "Users",
                column: "PersonKey",
                principalTable: "Persons",
                principalColumn: "Key",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Claims_Users_Key",
                table: "Claims");

            migrationBuilder.DropForeignKey(
                name: "FK_Claims_Users_UserKey",
                table: "Claims");

            migrationBuilder.DropForeignKey(
                name: "FK_Persons_Addresses_AddressKey",
                table: "Persons");

            migrationBuilder.DropForeignKey(
                name: "FK_Roles_Users_Key",
                table: "Roles");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Persons_PersonKey",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_PersonKey",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "DateRegistration",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "PersonKey",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "UserType",
                table: "Users");

            migrationBuilder.AlterColumn<Guid>(
                name: "AddressKey",
                table: "Persons",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "UserKey",
                table: "Claims",
                type: "uniqueidentifier",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.AddForeignKey(
                name: "FK_Claims_Users_UserKey",
                table: "Claims",
                column: "UserKey",
                principalTable: "Users",
                principalColumn: "Key",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Persons_Addresses_AddressKey",
                table: "Persons",
                column: "AddressKey",
                principalTable: "Addresses",
                principalColumn: "Key",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
