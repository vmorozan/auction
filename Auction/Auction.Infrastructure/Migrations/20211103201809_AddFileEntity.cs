﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Auction.Infrastructure.Migrations
{
    public partial class AddFileEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AdvertismentRealEstates_Filters_StateKey",
                table: "AdvertismentRealEstates");

            migrationBuilder.DropForeignKey(
                name: "FK_AdvertismentRealEstates_Regions_RegionKey",
                table: "AdvertismentRealEstates");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_ExecutorDetails_ExecutorDetailsKey",
                table: "Users");

            migrationBuilder.DropTable(
                name: "Blobs");

            migrationBuilder.DropTable(
                name: "ExecutorDetails");

            migrationBuilder.DropIndex(
                name: "IX_AdvertismentRealEstates_RegionKey",
                table: "AdvertismentRealEstates");

            migrationBuilder.DropColumn(
                name: "Comments",
                table: "AdvertismentRealEstates");

            migrationBuilder.DropColumn(
                name: "Price",
                table: "AdvertismentRealEstates");

            migrationBuilder.DropColumn(
                name: "RegionKey",
                table: "AdvertismentRealEstates");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "AdvertismentRealEstates");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "AdvertismentRealEstates");

            migrationBuilder.RenameColumn(
                name: "StateKey",
                table: "AdvertismentRealEstates",
                newName: "SequesterKey");

            migrationBuilder.RenameIndex(
                name: "IX_AdvertismentRealEstates_StateKey",
                table: "AdvertismentRealEstates",
                newName: "IX_AdvertismentRealEstates_SequesterKey");

            migrationBuilder.AddColumn<Guid>(
                name: "SequesterKey",
                table: "AdvertismentCars",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Executors",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ExecutorNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Executors", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "Sequesters",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Type = table.Column<int>(type: "int", nullable: false),
                    StateKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Comments = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RegionKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    MarkedPrice = table.Column<decimal>(type: "decimal(12,2)", nullable: false),
                    BuyedPrice = table.Column<decimal>(type: "decimal(12,2)", nullable: false),
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sequesters", x => x.Key);
                    table.ForeignKey(
                        name: "FK_Sequesters_Filters_StateKey",
                        column: x => x.StateKey,
                        principalTable: "Filters",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Sequesters_Regions_RegionKey",
                        column: x => x.RegionKey,
                        principalTable: "Regions",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Files",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    MimeType = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    OrderedId = table.Column<int>(type: "int", nullable: false),
                    FileType = table.Column<byte[]>(type: "varbinary(max)", nullable: true),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: true),
                    SequesterKey = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ExecutorKey = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Files", x => x.Key);
                    table.ForeignKey(
                        name: "FK_Files_Executors_ExecutorKey",
                        column: x => x.ExecutorKey,
                        principalTable: "Executors",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Files_Sequesters_SequesterKey",
                        column: x => x.SequesterKey,
                        principalTable: "Sequesters",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentCars_SequesterKey",
                table: "AdvertismentCars",
                column: "SequesterKey",
                unique: true,
                filter: "[SequesterKey] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Files_ExecutorKey",
                table: "Files",
                column: "ExecutorKey");

            migrationBuilder.CreateIndex(
                name: "IX_Files_SequesterKey",
                table: "Files",
                column: "SequesterKey");

            migrationBuilder.CreateIndex(
                name: "IX_Sequesters_RegionKey",
                table: "Sequesters",
                column: "RegionKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Sequesters_StateKey",
                table: "Sequesters",
                column: "StateKey",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_AdvertismentCars_Sequesters_SequesterKey",
                table: "AdvertismentCars",
                column: "SequesterKey",
                principalTable: "Sequesters",
                principalColumn: "Key",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AdvertismentRealEstates_Sequesters_SequesterKey",
                table: "AdvertismentRealEstates",
                column: "SequesterKey",
                principalTable: "Sequesters",
                principalColumn: "Key",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Executors_ExecutorDetailsKey",
                table: "Users",
                column: "ExecutorDetailsKey",
                principalTable: "Executors",
                principalColumn: "Key",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AdvertismentCars_Sequesters_SequesterKey",
                table: "AdvertismentCars");

            migrationBuilder.DropForeignKey(
                name: "FK_AdvertismentRealEstates_Sequesters_SequesterKey",
                table: "AdvertismentRealEstates");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Executors_ExecutorDetailsKey",
                table: "Users");

            migrationBuilder.DropTable(
                name: "Files");

            migrationBuilder.DropTable(
                name: "Executors");

            migrationBuilder.DropTable(
                name: "Sequesters");

            migrationBuilder.DropIndex(
                name: "IX_AdvertismentCars_SequesterKey",
                table: "AdvertismentCars");

            migrationBuilder.DropColumn(
                name: "SequesterKey",
                table: "AdvertismentCars");

            migrationBuilder.RenameColumn(
                name: "SequesterKey",
                table: "AdvertismentRealEstates",
                newName: "StateKey");

            migrationBuilder.RenameIndex(
                name: "IX_AdvertismentRealEstates_SequesterKey",
                table: "AdvertismentRealEstates",
                newName: "IX_AdvertismentRealEstates_StateKey");

            migrationBuilder.AddColumn<string>(
                name: "Comments",
                table: "AdvertismentRealEstates",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Price",
                table: "AdvertismentRealEstates",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<Guid>(
                name: "RegionKey",
                table: "AdvertismentRealEstates",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "AdvertismentRealEstates",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "AdvertismentRealEstates",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "ExecutorDetails",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ExecutorNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExecutorDetails", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "Blobs",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ExecutorDetailKey = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ImageData = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    KeyEntity = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    MimeType = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Blobs", x => x.Key);
                    table.ForeignKey(
                        name: "FK_Blobs_ExecutorDetails_ExecutorDetailKey",
                        column: x => x.ExecutorDetailKey,
                        principalTable: "ExecutorDetails",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentRealEstates_RegionKey",
                table: "AdvertismentRealEstates",
                column: "RegionKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Blobs_ExecutorDetailKey",
                table: "Blobs",
                column: "ExecutorDetailKey");

            migrationBuilder.AddForeignKey(
                name: "FK_AdvertismentRealEstates_Filters_StateKey",
                table: "AdvertismentRealEstates",
                column: "StateKey",
                principalTable: "Filters",
                principalColumn: "Key",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AdvertismentRealEstates_Regions_RegionKey",
                table: "AdvertismentRealEstates",
                column: "RegionKey",
                principalTable: "Regions",
                principalColumn: "Key",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_ExecutorDetails_ExecutorDetailsKey",
                table: "Users",
                column: "ExecutorDetailsKey",
                principalTable: "ExecutorDetails",
                principalColumn: "Key",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
