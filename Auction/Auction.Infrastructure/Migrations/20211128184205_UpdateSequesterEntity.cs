﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Auction.Infrastructure.Migrations
{
    public partial class UpdateSequesterEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Sequesters_Filters_StateKey",
                table: "Sequesters");

            migrationBuilder.DropForeignKey(
                name: "FK_Sequesters_Regions_RegionKey",
                table: "Sequesters");

            migrationBuilder.DropTable(
                name: "AdvertismentCars");

            migrationBuilder.DropIndex(
                name: "IX_Sequesters_RegionKey",
                table: "Sequesters");

            migrationBuilder.RenameColumn(
                name: "StateKey",
                table: "Sequesters",
                newName: "SubCategoryKey");

            migrationBuilder.RenameColumn(
                name: "RegionKey",
                table: "Sequesters",
                newName: "CreatorKey");

            migrationBuilder.RenameIndex(
                name: "IX_Sequesters_StateKey",
                table: "Sequesters",
                newName: "IX_Sequesters_SubCategoryKey");

            migrationBuilder.AddColumn<Guid>(
                name: "CategoryKey",
                table: "Sequesters",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<DateTime>(
                name: "DateExpired",
                table: "Sequesters",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DateRegistration",
                table: "Sequesters",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<Guid>(
                name: "SequesterKey",
                table: "Filters",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "SequesterKey1",
                table: "Filters",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Sequesters_CategoryKey",
                table: "Sequesters",
                column: "CategoryKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Sequesters_CreatorKey",
                table: "Sequesters",
                column: "CreatorKey");

            migrationBuilder.CreateIndex(
                name: "IX_Filters_SequesterKey",
                table: "Filters",
                column: "SequesterKey");

            migrationBuilder.CreateIndex(
                name: "IX_Filters_SequesterKey1",
                table: "Filters",
                column: "SequesterKey1");

            migrationBuilder.AddForeignKey(
                name: "FK_Filters_Sequesters_SequesterKey",
                table: "Filters",
                column: "SequesterKey",
                principalTable: "Sequesters",
                principalColumn: "Key",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Filters_Sequesters_SequesterKey1",
                table: "Filters",
                column: "SequesterKey1",
                principalTable: "Sequesters",
                principalColumn: "Key",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Sequesters_Categories_CategoryKey",
                table: "Sequesters",
                column: "CategoryKey",
                principalTable: "Categories",
                principalColumn: "Key",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Sequesters_SubCategories_SubCategoryKey",
                table: "Sequesters",
                column: "SubCategoryKey",
                principalTable: "SubCategories",
                principalColumn: "Key",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Sequesters_Users_CreatorKey",
                table: "Sequesters",
                column: "CreatorKey",
                principalTable: "Users",
                principalColumn: "Key",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Filters_Sequesters_SequesterKey",
                table: "Filters");

            migrationBuilder.DropForeignKey(
                name: "FK_Filters_Sequesters_SequesterKey1",
                table: "Filters");

            migrationBuilder.DropForeignKey(
                name: "FK_Sequesters_Categories_CategoryKey",
                table: "Sequesters");

            migrationBuilder.DropForeignKey(
                name: "FK_Sequesters_SubCategories_SubCategoryKey",
                table: "Sequesters");

            migrationBuilder.DropForeignKey(
                name: "FK_Sequesters_Users_CreatorKey",
                table: "Sequesters");

            migrationBuilder.DropIndex(
                name: "IX_Sequesters_CategoryKey",
                table: "Sequesters");

            migrationBuilder.DropIndex(
                name: "IX_Sequesters_CreatorKey",
                table: "Sequesters");

            migrationBuilder.DropIndex(
                name: "IX_Filters_SequesterKey",
                table: "Filters");

            migrationBuilder.DropIndex(
                name: "IX_Filters_SequesterKey1",
                table: "Filters");

            migrationBuilder.DropColumn(
                name: "CategoryKey",
                table: "Sequesters");

            migrationBuilder.DropColumn(
                name: "DateExpired",
                table: "Sequesters");

            migrationBuilder.DropColumn(
                name: "DateRegistration",
                table: "Sequesters");

            migrationBuilder.DropColumn(
                name: "SequesterKey",
                table: "Filters");

            migrationBuilder.DropColumn(
                name: "SequesterKey1",
                table: "Filters");

            migrationBuilder.RenameColumn(
                name: "SubCategoryKey",
                table: "Sequesters",
                newName: "StateKey");

            migrationBuilder.RenameColumn(
                name: "CreatorKey",
                table: "Sequesters",
                newName: "RegionKey");

            migrationBuilder.RenameIndex(
                name: "IX_Sequesters_SubCategoryKey",
                table: "Sequesters",
                newName: "IX_Sequesters_StateKey");

            migrationBuilder.CreateTable(
                name: "AdvertismentCars",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CategoryKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Comments = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatorKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DateExpired = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateRegistration = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MarkKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    MatriculateKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ModelKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Price = table.Column<double>(type: "float", nullable: false),
                    RegionKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SequesterKey = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    StateKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    SubCategoryKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Type = table.Column<int>(type: "int", nullable: false),
                    WheelKey = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvertismentCars", x => x.Key);
                    table.ForeignKey(
                        name: "FK_AdvertismentCars_Categories_CategoryKey",
                        column: x => x.CategoryKey,
                        principalTable: "Categories",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentCars_Countries_MatriculateKey",
                        column: x => x.MatriculateKey,
                        principalTable: "Countries",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentCars_Filters_MarkKey",
                        column: x => x.MarkKey,
                        principalTable: "Filters",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentCars_Filters_ModelKey",
                        column: x => x.ModelKey,
                        principalTable: "Filters",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentCars_Filters_StateKey",
                        column: x => x.StateKey,
                        principalTable: "Filters",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentCars_Filters_WheelKey",
                        column: x => x.WheelKey,
                        principalTable: "Filters",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentCars_Regions_RegionKey",
                        column: x => x.RegionKey,
                        principalTable: "Regions",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentCars_Sequesters_SequesterKey",
                        column: x => x.SequesterKey,
                        principalTable: "Sequesters",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentCars_SubCategories_SubCategoryKey",
                        column: x => x.SubCategoryKey,
                        principalTable: "SubCategories",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertismentCars_Users_CreatorKey",
                        column: x => x.CreatorKey,
                        principalTable: "Users",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Sequesters_RegionKey",
                table: "Sequesters",
                column: "RegionKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentCars_CategoryKey",
                table: "AdvertismentCars",
                column: "CategoryKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentCars_CreatorKey",
                table: "AdvertismentCars",
                column: "CreatorKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentCars_MarkKey",
                table: "AdvertismentCars",
                column: "MarkKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentCars_MatriculateKey",
                table: "AdvertismentCars",
                column: "MatriculateKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentCars_ModelKey",
                table: "AdvertismentCars",
                column: "ModelKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentCars_RegionKey",
                table: "AdvertismentCars",
                column: "RegionKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentCars_SequesterKey",
                table: "AdvertismentCars",
                column: "SequesterKey",
                unique: true,
                filter: "[SequesterKey] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentCars_StateKey",
                table: "AdvertismentCars",
                column: "StateKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentCars_SubCategoryKey",
                table: "AdvertismentCars",
                column: "SubCategoryKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertismentCars_WheelKey",
                table: "AdvertismentCars",
                column: "WheelKey",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Sequesters_Filters_StateKey",
                table: "Sequesters",
                column: "StateKey",
                principalTable: "Filters",
                principalColumn: "Key",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Sequesters_Regions_RegionKey",
                table: "Sequesters",
                column: "RegionKey",
                principalTable: "Regions",
                principalColumn: "Key",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
