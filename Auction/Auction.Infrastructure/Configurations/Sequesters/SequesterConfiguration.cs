﻿using Auction.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Infrastructure.Configurations.Sequesters
{
    public class SequesterConfiguration : IEntityTypeConfiguration<Sequester>
    {
        public void Configure(EntityTypeBuilder<Sequester> builder)
        {
            builder.HasMany(x => x.Files).WithOne(x => x.Sequester)
                .OnDelete(DeleteBehavior.ClientCascade);
            builder.HasOne(x => x.Category).WithOne()
                .HasForeignKey<Sequester>(x => x.CategoryKey)
                .OnDelete(DeleteBehavior.ClientCascade)
                .IsRequired(false);
            builder.HasOne(x => x.SubCategory).WithOne()
                .HasForeignKey<Sequester>(x => x.SubCategoryKey)
                .OnDelete(DeleteBehavior.ClientCascade);
            builder.HasIndex(x => x.CategoryKey).IsUnique(false);
            builder.HasIndex(x => x.SubCategoryKey).IsUnique(false);
        }
    }
}
