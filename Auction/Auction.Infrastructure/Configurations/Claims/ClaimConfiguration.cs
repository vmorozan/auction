﻿
using Auction.Domain.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Auction.Infrastructure.Configurations.Claims
{
    public class ClaimConfiguration : IEntityTypeConfiguration<Claim>
    {
        public void Configure(EntityTypeBuilder<Claim> builder)
        {
            builder.HasOne(c => c.User).WithMany()
               .HasForeignKey(x => x.Key)
               .OnDelete(DeleteBehavior.ClientCascade);
        }

    }
}
