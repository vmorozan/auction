﻿using Auction.Domain.Persons;
using Auction.Domain.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Infrastructure.Users.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(p => p.Id).ValueGeneratedOnAdd();
            builder.Property(p => p.Key).ValueGeneratedOnAdd();
            builder.HasOne(c => c.Person).WithOne()
                .HasForeignKey<User>(x => x.PersonKey)
                .OnDelete(DeleteBehavior.ClientCascade);
            builder.Property(x => x.IsDeleted).HasDefaultValue(false);
            //builder.HasMany(c => c.Claims).WithOne()
            //    .HasForeignKey(x => x.Key);
            //builder.HasMany(c => c.Roles).WithOne()
            //    .HasForeignKey(x => x.Key);
        }
    }
}
