﻿using Auction.Domain.Executors;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Infrastructure.Configurations.Executors
{
    public class ExecutorConfiguration : IEntityTypeConfiguration<Executor>
    {
        public void Configure(EntityTypeBuilder<Executor> builder)
        {
            builder.HasMany(x => x.Files).WithOne(x => x.Executor)
                .OnDelete(DeleteBehavior.ClientCascade);
        }
    }
}
