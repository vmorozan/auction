﻿using Auction.Domain.Roles;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Infrastructure.Configurations.Roles
{
    public class RoleConfiguration : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.Property(p => p.Id).ValueGeneratedOnAdd();
            builder.Property(p => p.Key).ValueGeneratedOnAdd();
            builder.HasMany(c => c.Claims).WithOne()
                .HasForeignKey(x => x.Key);
            builder.HasOne(c => c.User).WithMany()
               .HasForeignKey(x => x.Key)
               .OnDelete(DeleteBehavior.ClientCascade);
        }
    }
}
