﻿using Auction.Domain.Filters;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Auction.Infrastructure.Configurations.Filters
{
    public class SequesterFiltersConfiguration : IEntityTypeConfiguration<CommonFiltersSequester>
    {
        public void Configure(EntityTypeBuilder<CommonFiltersSequester> builder)
        {
            builder.HasOne(x => x.Sequester)
                .WithMany(x => x.Filters)
                .HasForeignKey(x => x.SequesterKey);

            builder.HasOne(x => x.Filter)
              .WithMany(x => x.Sequester)
              .HasForeignKey(x => x.FilterKey);
        }
    }
}
