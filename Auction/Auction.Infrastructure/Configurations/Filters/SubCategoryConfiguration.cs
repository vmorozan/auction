﻿using Auction.Domain.Filters;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Auction.Infrastructure.Configurations.Filters
{
    public class SubCategoryConfiguration : IEntityTypeConfiguration<SubCategory>
    {
        public void Configure(EntityTypeBuilder<SubCategory> builder)
        {
            builder.Property(p => p.Id).ValueGeneratedOnAdd();
            builder.Property(p => p.Key).ValueGeneratedOnAdd();
            builder.HasOne(x => x.Category).WithMany()
                .HasForeignKey(x => x.CategoryKey);
        }
    }
}
