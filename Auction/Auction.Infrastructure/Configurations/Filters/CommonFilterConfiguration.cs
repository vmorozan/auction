﻿using Auction.Domain.Filters;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Auction.Infrastructure.Configurations.Filters
{
    public class CommonFilterConfiguration : IEntityTypeConfiguration<CommonFilters>
    {
        public void Configure(EntityTypeBuilder<CommonFilters> builder)
        {
            builder.Property(p => p.Id).ValueGeneratedOnAdd();
            builder.Property(p => p.Key).ValueGeneratedOnAdd();
            builder.HasOne(x => x.Category)
                .WithMany()
                .HasForeignKey(x => x.CategoryKey);
            builder.HasOne(x => x.SubCategory)
                .WithMany()
                .HasForeignKey(x => x.SubCategoryKey);
        }
    }
}
