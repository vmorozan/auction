﻿using Auction.Data;
using Auction.Domain.Addresses;
using Auction.Domain.Filters;
using Auction.Domain.Users;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Infrastructure
{
    public class AuctionDbSeed
    {
        public static async Task SeedAsync(AuctionDbContext context, ILoggerFactory loggerFactory, UserManager<User> userManager)
        {
            try
            {
                if (!context.Countries.Any())
                {
                    var countries = new List<Country>
                    {
				        new Country { Key = Guid.Parse("c9649d9a-1c5e-4bd5-a7c9-f5e43ffbca0a"), Name = "Moldova" },
				        new Country { Key = Guid.Parse("3fcc843f-064e-426c-aec3-c5dbb04037a8"),Name = "Transnistria" },
				        new Country { Key = Guid.Parse("e03ed015-db89-4670-a422-14ec14a7fdf3"),Name = "Other" }
                    };
                    await context.AddRangeAsync(countries);
                    await context.SaveChangesAsync();
                }

                if (!context.Regions.Any())
                {
                    var regions = new List<Region>
                    {
                        new Region { Key = Guid.Parse("c9649d9a-1c5e-4bd5-a7c9-f5e43ffbca0a"), Name = "Cahul" },
                        new Region { Key = Guid.Parse("3fcc843f-064e-426c-aec3-c5dbb04037a8"), Name = "mun. Chisinau" },
                        new Region { Key = Guid.Parse("e03ed015-db89-4670-a422-14ec14a7fdf3"), Name = "Calarasi" }
                    };
                    await context.AddRangeAsync(regions);
                    await context.SaveChangesAsync();
                }
                if (!context.Cities.Any())
                {
                    var regions = new List<City>
                    {
                        new City { Key = Guid.Parse("c9649d9a-1c5e-4bd5-a7c9-f5e43ffbca0a"), Name = "Cahul" },
                        new City { Key = Guid.Parse("3fcc843f-064e-426c-aec3-c5dbb04037a8"), Name = "Chisinau" },
                        new City { Key = Guid.Parse("e03ed015-db89-4670-a422-14ec14a7fdf3"), Name = "Calarasi" },
                        new City { Key = Guid.Parse("052858d5-f21a-453a-a99f-ed39381c62a4"), Name = "Balti" },
                        new City { Key = Guid.Parse("ef6505dd-ff57-4ae7-9186-5738f32f4239"), Name = "Edinet" },
                        new City { Key = Guid.Parse("b9d5b403-272a-4c23-8572-92c81b67f8e3"), Name = "Soroca" }
                    };
                    await context.AddRangeAsync(regions);
                    await context.SaveChangesAsync();
                }

                if (!context.Categories.Any())
                {
                    var categories = new List<Category> { 
                        new Category { Key = Guid.Parse("827fa7af-448a-4711-aea6-bd26a1929510"), Name = "Transport"},
                        new Category { Key = Guid.Parse("db90f0cd-4d53-46e4-9344-5e259d44f53c"), Name = "Imobiliare"}
                    };
                    await context.AddRangeAsync(categories);
                    await context.SaveChangesAsync();
                }

                if (!context.SubCategories.Any())
                {
                    var subcategories = new List<SubCategory> {
                        new SubCategory {
                            Key = Guid.Parse("7c3f1940-66b9-4225-a45f-79cbc07c9416"),
                            CategoryKey = Guid.Parse("827fa7af-448a-4711-aea6-bd26a1929510"),
                            Name = "Autoturisme"},
                        new SubCategory {
                            Key = Guid.Parse("00ba0912-4893-4db1-81e8-86e2cc5eaaf0"),
                            CategoryKey = Guid.Parse("827fa7af-448a-4711-aea6-bd26a1929510"),
                            Name = "Autobuze si microbuze"},
                        new SubCategory {
                            Key = Guid.Parse("79ab9ae4-c4c0-4a6e-b6dc-69794ff1354b"),
                            CategoryKey = Guid.Parse("db90f0cd-4d53-46e4-9344-5e259d44f53c"),
                            Name = "Apartamente"},
                        new SubCategory {
                            Key = Guid.Parse("48c5475e-b84b-47e3-963e-7eaec1d31021"),
                            CategoryKey = Guid.Parse("db90f0cd-4d53-46e4-9344-5e259d44f53c"),
                            Name = "Loturi de teren"}
                    };
                    await context.AddRangeAsync(subcategories);
                    await context.SaveChangesAsync();
                }

                if (!context.Filters.Any())
                {
                    var filters = new List<CommonFilters>
                    {
                        new CommonFilters {
                            Key = Guid.Parse("a13f7bcc-5fd2-4d77-8419-966171649612"), 
                            CategoryKey = Guid.Parse("827fa7af-448a-4711-aea6-bd26a1929510"),
                            Value="Audi" ,  
                            Name = "Marca" },
                        new CommonFilters {
                            Key = Guid.Parse("75cb593a-8688-4a0e-81b5-c6605f8e2edf"),
                            CategoryKey = Guid.Parse("827fa7af-448a-4711-aea6-bd26a1929510"),
                            Value="BMW" ,
                            Name = "Marca" },
                        new CommonFilters {
                            Key = Guid.Parse("bff42ba9-0056-4d2f-8072-f794061e87d1"),
                            CategoryKey = Guid.Parse("827fa7af-448a-4711-aea6-bd26a1929510"),
                            Value="Mercedes-Benz",
                            Name = "Marca" },
                        new CommonFilters {
                            Key = Guid.Parse("0ed8d698-4b8d-4702-9c47-63ca1f7b6ee0"),
                            Value="Moldova",
                            Name = "Înmatriculat" },
                        new CommonFilters {
                            Key = Guid.Parse("a831e181-e50a-44a0-a1ba-1db0f4d01f20"),
                            Value="Transnistria",
                            Name = "Înmatriculat" },
                        new CommonFilters {
                            Key = Guid.Parse("6b27f51a-ed2d-42ff-a92c-4c2173efb088"),
                            Value="Alte",
                            Name = "Înmatriculat" },
                        new CommonFilters {
                            Key = Guid.Parse("3f0cf00c-8606-4fcb-828e-d7f80e9d59bb"),
                            Value="Nou",
                            Name = "Stare" },
                        new CommonFilters {
                            Key = Guid.Parse("d89b580b-b566-4ac5-b0df-a89251be1a99"),
                            Value="Cu rulaj",
                            Name = "Stare" },
                        new CommonFilters {
                            Key = Guid.Parse("8c3a6b46-39a9-41d1-af0e-14591e035be4"),
                            Value="Necesita reparatia",
                            Name = "Stare" },
                        new CommonFilters {
                            Key = Guid.Parse("070b0791-2c27-46c4-877e-3338ba201495"),
                            CategoryKey = Guid.Parse("827fa7af-448a-4711-aea6-bd26a1929510"),
                            SubCategoryKey = Guid.Parse("7c3f1940-66b9-4225-a45f-79cbc07c9416"),
                            Value="Stanga",
                            Name = "Volan" },
                        new CommonFilters {
                            Key = Guid.Parse("e6075699-e8a9-42c7-a61c-1ca3d6ada51e"),
                            CategoryKey = Guid.Parse("827fa7af-448a-4711-aea6-bd26a1929510"),
                            SubCategoryKey = Guid.Parse("7c3f1940-66b9-4225-a45f-79cbc07c9416"),
                            Value="Dreapta",
                            Name = "Volan" },
                    };
                    await context.AddRangeAsync(filters);
                    await context.SaveChangesAsync();
                }

                if (!userManager.Users.Any())
                {
                    var users = new User
                    {
                        Key = Guid.Parse("a13f7bcc-5fd2-4d77-8419-966171649612"),
                        UserName = "v.morozan",
                        IsDeleted = false,
                        DateRegistration = DateTime.Now,
                        Email = "vlad6morozan@gmail.com",
                    };

                    await userManager.CreateAsync(users, "Vlad_890!!");
                }
            }
            catch (Exception ex)
            {
                var logger = loggerFactory.CreateLogger<AuctionDbContext>();
                logger.LogError(ex.Message);
            }
        }
    }
}
