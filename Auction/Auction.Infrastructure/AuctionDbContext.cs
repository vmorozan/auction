﻿using Auction.Domain;
using Auction.Domain.Addresses;
using Auction.Domain.Cars;
using Auction.Domain.Executors;
using Auction.Domain.Files;
using Auction.Domain.Filters;
using Auction.Domain.Persons;
using Auction.Domain.RealEstates;
using Auction.Domain.Roles;
using Auction.Domain.Users;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Auction.Data
{
    public class AuctionDbContext : DbContext
    {
        public AuctionDbContext(DbContextOptions<AuctionDbContext> options) : base (options)
        {

        }

        public DbSet<User> Users { get; set; }

        public DbSet<Claim> Claims { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Person> Persons { get; set; }

        public DbSet<Address> Addresses { get; set; }

        public DbSet<Country> Countries { get; set; }

        public DbSet<Region> Regions { get; set; }

        public DbSet<City> Cities { get; set; }

        public DbSet<CommonFilters> Filters { get; set; }

        public DbSet<SubCategory> SubCategories { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<FileEntity> Files { get; set; }

        public DbSet<Executor> Executors { get; set; }

        public DbSet<Sequester> Sequesters { get; set; }

        public DbSet<CommonFiltersSequester> SequesterFilters { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}
