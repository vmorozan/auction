﻿using Auction.Data;
using Auction.Domain.Others;
using Auction.Domain.Users;
using Auction.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Infrastructure.Repositories
{
    public class ExecutorRepository : IExecutorRepository
    {
        private readonly AuctionDbContext _context;
        private readonly UserManager<User> _userManager;


        public ExecutorRepository(AuctionDbContext context, UserManager<User> userManager)
        {
            _userManager = userManager;
            _context = context;
        }

        public async Task<bool> Add(User executor)
        {
            try
            {
                if (executor.Person != null)
                {
                    if (executor.Person.Address != null)
                        await _context.Addresses.AddAsync(executor.Person.Address);

                    await _context.Persons.AddAsync(executor.Person);
                }

                if (executor.ExecutorDetails != null)
                    await _context.Executors.AddAsync(executor.ExecutorDetails);

                await _context.SaveChangesAsync();

                await _userManager.CreateAsync(executor);

                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public Task<bool> Delete(Guid key)
        {
            throw new NotImplementedException();
        }

        public IQueryable<User> Gets()
        {
            return _context.Users.AsNoTracking()
                .Where(x => x.UserType == UserType.Executor)
                .Include(x => x.ExecutorDetails)
                .Include(x => x.Person)
                    .ThenInclude(a => a.Address);
        }

        public Task<User> Update(User executor)
        {
            throw new NotImplementedException();
        }
    }
}
