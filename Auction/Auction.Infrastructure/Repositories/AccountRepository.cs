﻿using Auction.Data;
using Auction.Domain.Users;
using Auction.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace Auction.Infrastructure.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        private readonly AuctionDbContext _context;
        private readonly UserManager<User> _userManager;


        public AccountRepository(AuctionDbContext context, UserManager<User> userManager)
        {
            _userManager = userManager;
            _context = context;
        }

        public async Task<User> Register(User user)
        {
            if(user.Person != null)
            {
                if (user.Person.Address != null)
                    await _context.Addresses.AddAsync(user.Person.Address);

                await _context.Persons.AddAsync(user.Person);
            }
            await _context.SaveChangesAsync();

            await _userManager.CreateAsync(user);

            return user;
        }
    }
}
