﻿using Auction.Data;
using Auction.Domain.Addresses;
using Auction.Domain.Filters;
using Auction.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auction.Infrastructure.Repositories
{
    public class FilterRepository : IFilterRepository
    {
        private readonly AuctionDbContext _context;

        public FilterRepository(AuctionDbContext context)
        {
            _context = context;
        }

        public async Task<Category> AddCategory(Category category)
        {
            try
            {
                await _context.Categories.AddAsync(category);
                await _context.SaveChangesAsync();
                return category;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<SubCategory> AddSubCategory(SubCategory subCategory)
        {
            try
            {
                await _context.SubCategories.AddAsync(subCategory);
                await _context.SaveChangesAsync();
                return subCategory;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<Category>> GetCategories()
        {
            return await _context.Categories.ToListAsync();
        }

        public async Task<IReadOnlyList<City>> GetCities()
        {
            return await _context.Cities.ToListAsync();
        }

        public async Task<Category> GetCategoryByName(string name)
        {
            return await _context.Categories.Where(x => x.Name == name).FirstOrDefaultAsync();
        }

        public async Task<IReadOnlyList<SubCategory>> GetSubCategories(Guid categoryKey)
        {
            return await _context.SubCategories.Where(x => x.CategoryKey == categoryKey).Include(x => x.Category).ToListAsync();
        }

        public async Task<CommonFilters> AddCommonFilter(CommonFilters commonFilters)
        {
            try
            {
                await _context.Filters.AddAsync(commonFilters);
                await _context.SaveChangesAsync();
                return commonFilters;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public IQueryable<CommonFilters> GetsCommonFilter()
        {
            return _context.Filters
                .AsNoTracking()
                .Include(x => x.Category)
                .Include(x => x.SubCategory);
        }

        public IQueryable<Category> GetListCategories()
        {
            return _context.Categories.AsNoTracking();
        }

        public IQueryable<SubCategory> GetListSubCategories()
        {
            return _context.SubCategories.AsNoTracking().Include(x => x.Category);
        }

        public IQueryable<CommonFilters> GetListCommonFilter(CommonFilters commonFilters)
        {
            return _context.Filters
                .AsNoTracking()
                .Where(x => x.CategoryKey == null && x.SubCategoryKey == null);
        }

        public IQueryable<CommonFilters> GetListCommonFilterByCategory(Guid categoryKey)
        {
            return _context.Filters
                .AsNoTracking()
                .Where(x => x.CategoryKey == categoryKey && x.SubCategoryKey == null);
        }

        public IQueryable<CommonFilters> GetListCommonFilterBySubCategory(Guid categoryKey, Guid subCategoryKey)
        {
            return _context.Filters
                .AsNoTracking()
                .Where(x => x.CategoryKey == categoryKey && x.SubCategoryKey == subCategoryKey);
        }
    }
}
