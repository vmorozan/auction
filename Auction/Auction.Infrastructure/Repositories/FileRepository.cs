﻿using Auction.Data;
using Auction.Domain.Files;
using Auction.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auction.Infrastructure.Repositories
{
    public class FileRepository : IFileRepository
    {
        private readonly AuctionDbContext _context;

        public FileRepository(AuctionDbContext context)
        {
            _context = context;
        }

        public IQueryable<FileEntity> Preview(Guid sequesterKey)
        {
            return _context.Files.Where(x => x.Key == sequesterKey);
        }

        public async Task<FileEntity> Upload(FileEntity file)
        {
            try
            {
                await _context.Files.AddAsync(file);
                await _context.SaveChangesAsync();
                return file;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
