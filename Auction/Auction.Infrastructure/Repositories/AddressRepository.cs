﻿using Auction.Data;
using Auction.Domain.Addresses;
using Auction.Infrastructure.Interfaces;
using System;
using System.Threading.Tasks;

namespace Auction.Infrastructure.Repositories
{
    public class AddressRepository : IAddressRepository
    {

        private readonly AuctionDbContext _context;

        public AddressRepository(AuctionDbContext context)
        {
            _context = context;
        }

        public async Task<Address> AddAsync(Address person)
        {
            await _context.Addresses.AddAsync(person);
            await _context.SaveChangesAsync();
            return person;
        }

        public async Task<bool> DeleteAsync(Address person)
        {
            try
            {
                _context.Addresses.Remove(person);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<Address> UpdateAsync(Address person)
        {
            _context.Addresses.Update(person);
            await _context.SaveChangesAsync();
            return person;
        }
    }
}
