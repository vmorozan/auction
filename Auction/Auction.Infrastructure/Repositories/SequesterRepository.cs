﻿using Auction.Data;
using Auction.Domain;
using Auction.Domain.Filters;
using Auction.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.Infrastructure.Repositories
{
    public class SequesterRepository : ISequesterRepository
    {
        private readonly AuctionDbContext _context;

        public SequesterRepository(AuctionDbContext context)
        {
            _context = context;
        }

        public async Task<Sequester> Create(Sequester sequester)
        {
            try
            {
                await _context.Sequesters.AddAsync(sequester);
                await _context.SaveChangesAsync();
                return sequester;
            }
            catch (Exception ex)
            {
                return sequester;
            }
        }

        public async Task<Sequester> AddFilters(Sequester sequester)
        {
            try
            {
                var seq = await _context.Sequesters.FirstOrDefaultAsync(x => x.Key == sequester.Key);
                seq.Filters = sequester.Filters;
                await _context.SaveChangesAsync();
                return sequester;

            }
            catch (Exception ex)
            {
                return sequester;
            }
        }

        public IQueryable<Sequester> Gets() 
            => _context.Sequesters.AsNoTracking()
                .Include(x => x.Files)
                .Include(x => x.Filters)
                .ThenInclude(x => x.Filter)
                .ThenInclude(x => x.Category)
                .Include(x => x.Filters)
                .ThenInclude(x => x.Filter)
                .ThenInclude(x => x.SubCategory);

        public async Task<Sequester> Get(Guid sequesterKey)
            => await _context.Sequesters.AsNoTracking()
                .Include(x => x.Files)
                .Include(x => x.Filters)
                .ThenInclude(x => x.Filter)
                .ThenInclude(x => x.Category)
                .Include(x => x.Filters)
                .ThenInclude(x => x.Filter)
                .ThenInclude(x => x.SubCategory)
                .FirstOrDefaultAsync(x => x.Key == sequesterKey);
    }
}
