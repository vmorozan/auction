﻿using Auction.Data;
using Auction.Domain.Persons;
using Auction.Infrastructure.Interfaces;
using System;
using System.Threading.Tasks;

namespace Auction.Infrastructure.Repositories
{
    public class PersonRepository : IPersonRepository
    {
        private readonly AuctionDbContext _context;

        public PersonRepository(AuctionDbContext context)
        {
            _context = context;
        }

        public async Task<Person> AddAsync(Person person)
        {
            await _context.Persons.AddAsync(person);
            await _context.SaveChangesAsync();
            return person;
        }

        public async Task<bool> DeleteAsync(Person person)
        {
            try
            {
                _context.Persons.Remove(person);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<Person> UpdateAsync(Person person)
        {
            _context.Persons.Update(person);
            await _context.SaveChangesAsync();
            return person;
        }
    }
}
