﻿using Auction.Data;
using Auction.Domain.Users;
using Auction.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Auction.Infrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly AuctionDbContext _context;

        public UserRepository(AuctionDbContext context)
        {
            _context = context;
        }

        public async Task<bool> Delete(Guid key)
        {
            try
            {
                var user = await _context.Users
                    .Include(x => x.Person)
                    .ThenInclude(x => x.Address)
                    .FirstAsync(x => x.Key == key);

                if (user.PersonKey != null || user.PersonKey == Guid.Empty)
                {
                    if (user.Person.AddressKey != null || user.Person.AddressKey != Guid.Empty)
                        _context.Remove(await _context.Addresses.FirstOrDefaultAsync(x => x.Key == user.Person.AddressKey));

                    _context.Remove(await _context.Persons.FirstOrDefaultAsync(x => x.Key == user.PersonKey));
                }

                _context.Remove(await _context.Users.FirstOrDefaultAsync(x => x.Key == key));

                await _context.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public IQueryable<User> Gets()
        {
            return  _context.Users.AsNoTracking()
                .Include(x => x.Claims)
                .Include(x => x.Person)
                    .ThenInclude(a => a.Address)
                .Include(x => x.Roles);
        }
    }
}
